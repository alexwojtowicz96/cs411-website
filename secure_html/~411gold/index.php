<?php
	$cssDir = "css";
	$jsDir = "js";
	$imgDir = "img";
	$phpDir = "php";
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
	$card1 = (file_get_contents($phpDir . "/partials/card1.php"));
	$androidCarousel = (file_get_contents($phpDir . "/partials/android-carousel.php"));
?>
<!DOCTYPE html>
<html class="homepage">
	<title>Tutor Dash | Home</title>
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<div class="bg-overlay"></div>
		<section>
			<div class="hero-section">
				<div class="hero-overlay fadein-fast">
					<div class="hero-content">
						<p class="typewriter line-1 hero-title" id="hero-title">Find a Tutor. Be a Tutor.</p>
						<p class="hero-title mobile" id="hero-title">Find a Tutor.<br /> Be a Tutor.</p>
						<section class="subcontent flyin delay1">
							<p class="hero-subtitle" id="hero-subtitle">Bridging the gap between students looking for tutors and students looking to become tutors</p>
							<a href=<?php echo ("'" . $phpDir . "/pages/about-us/what-is-tutor-dash.php'") ?> class='btn hero-btn' id="hero-btn">Explore Tutor Dash</a>
						</section>
					</div>
				</div>
				<img class='hero-img' src=<?php echo ("'" . $imgDir . "/hero/students-on-phones.jpg'") ?> alt='college tutor technology' />
			</div>
		</section>
		<main>
			<div class="body">
				<div class="content">
 
					<!-- Add content here -->
					<section class="fluid-bg-1">
						<?php
							$firstCard = [
								"title" => "Understanding Our Purpose",
								"blurb" => "Tutor Dash is an Android application simlar to that of Uber, only it's for tutors!",
								"image" => "$imgDir/icons/understanding.png",
								"link" => "$phpDir/pages/about-us/what-is-tutor-dash.php",
							];
							$secondCard = [
								"title" => "Analyzing The Problem",
								"blurb" => "A adequate platform for college students to offer tutoring services and/or find tutors is lacking.",
								"image" => "$imgDir/icons/providing.png",
								"link" => "$phpDir/pages/about-us/the-problem.php",
							];
							$thirdCard = [
								"title" => "Providing The Best Solution",
								"blurb" => "We are here to bridge the gap and redefine how college students perceive tutoring.",
								"image" => "$imgDir/icons/solution.png",
								"link" => "$phpDir/pages/about-us/our-solution.php",
							];
							$cards = [
								$firstCard,
								$secondCard,
								$thirdCard,
							];
							echo render_card1_section($cards, $phpDir, $imgDir, $card1); 
						?>
					</section>

					<section>
						<div class='responsive-img'>
							<img src='img/mockup-2.jpg' alt='tutor dash mockup phone smartphone' />
							<div style="background-image: url('img/mockup-2.jpg')">
								<div class='overlay dark-left'>
									<div class='flyin'>
										<div class='content right-align'>
											<h2>The Uber for Tutors!</h2>
											<p>Tutor Dash is the best way for students to find tutoring services since it promotes student engagement by centralizing all private tutoring resources in one place!</p>
											<div class='btn-wrapper'>
												<a class='btn' href='php/pages/presentations/demonstrations/final-demo.php'>Learn What We're About</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<section>
						<div class='responsive-img'>
							<img src='img/mockup-2.jpg' alt='tutor dash mockup phone smartphone' />
							<div style="background-image: url('img/mockup-1.jpg')">
								<div class='overlay dark-right'>
									<div class='flyin'>
										<div class='content left-align'>
											<h2 class="deep-cove">Need Tutoring Help Fast? We've Got You Covered.</h2>
											<p>With Tutor Dash, you can schedule a session anywhere with anyone! Just log in, make yourself available, and you're all set!</p>
											<div class='btn-wrapper'>
												<a class='btn deep-cove' href='php/pages/development/download-app.php'>Download Now!</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>

					<section class="fluid-bg-2 has-gradient-bg">
						<div class='gradient-bg gradient1'></div>
							<?php 
								$heading = "How It Works";
								$subheading = "We are here to change the way college students perceive tutoring by offering a convenient platform for everyone's tutoring needs";
								$steps = [
									"step1" => "Sign up with Tutor Dash. Choose whether you would like to be a tutor, or just be a tutee. Keep in mind, you can always become a tutor later!",
									"step2" => "If you opted to just be a tutee, then you are all set! If you opted to become a tutor, then all you need is to provide your official academic transcript to our service.",
									"step3" => "Your academic transcript will be analyzed, and you will be qualified to tutor eligible courses instantly!",
									"step4" => "Our algorithms keep everyone's prices competitive, so you don't have to worry about setting your own prices.",
									"step5" => "That's it! Now, just set your availability and we will start connecting you with the people you are looking for!",
								];
								$images = [
									"img1" => $imgDir . "/carousel/signup.jpg",
									"img2" => $imgDir . "/carousel/map.jpg",
									"img3" => $imgDir . "/carousel/courses.jpg",
									"img4" => $imgDir . "/carousel/text-discovery.jpg",
									"img5" => $imgDir . "/carousel/session-scheduling.jpg",
								];
								$content = [
									$heading,
									$subheading,
									$steps,
									$images,
								];
								echo render_android_carousel_section($content, $phpDir, $imgDir, $androidCarousel);
							?>
					</section>

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>