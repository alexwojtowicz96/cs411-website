$(document).ready(function () {
	$('.carousel').carousel();
	if ($('div').hasClass('android-carousel-section')) {
		console.log('android-carousel-section is present!');
		ScrollReveal().reveal('.android-carousel-section', {
			delay: 500,
			easing: 'ease-in-out',
			origin: 'left',
			distance: '500px',
			duration: 1000,
			interval: 500
		});
		bindCarousel();
	}

	if ($('div').hasClass('risks-card')) {
		var overlay = $(this).find('.overlay-bg');
		overlay.click(function () {
			var overlayText = $(this).parent().find('.overlay > p');
			var hiddenSection = $(this).parent().parent().parent().find('.hidden-section');
			if (hiddenSection.hasClass('selected')) {
				hiddenSection.removeClass('selected');
				overlayText.html('Show More');
			}
			else {
				hiddenSection.addClass('selected');
				overlayText.html('Show Less');
			}
		});
	}

	if ($('div').hasClass('story-card')) {
		var card = $(this).find('.visible-part');
		card.click(function () {
			var hiddenPart = $(this).parent().find('.hidden-part');
			if (hiddenPart.hasClass('display-block')) {
				hiddenPart.removeClass('display-block')
			}
			else {
				hiddenPart.addClass('display-block');
			}
		});
	}
	
	matchCard1Heights();
	var homepageHeight = $('html').height();
	$('html.homepage body div.bg-overlay').css("height", (homepageHeight + 1) + "px");
	$(window).resize(function() {
		var homepageHeight = $('html').height();
		$('html.homepage body div.bg-overlay').css("height", (homepageHeight + 1) + "px");
		matchCard1Heights();
	});

	$('#mobileMenuButton').on('click', function () {
		if ($('#mobileMenuButton').hasClass("off")) { 
			displayMenu();
		}
		else  {
			hideMenu();
		}
	});

	$('.mobile-overlay').on('click', function () {
		console.log("Clicked Overlay...");
		if($(this).hasClass("visible")) {
			hideMenu();
		}
	});

	$('.link-enabled').click(function () {
		if ($(this).hasClass('new-tab')) {
			if ($(this).find('a').length) {
				window.open($(this).find('a:first').attr('href'));
			}
		}
		else {
			console.log("no 'new-tab'");
			if ($(this).find("a").length) {
				window.location.href = $(this).find("a:first").attr("href");
			}
		}
	});

	$('.mobile-item.dropdown').click(function () {
		console.log("clicked menu");
		$(this).addClass("target");
		var menus = $('.mobile-nav').find("> .mobile-item.dropdown");
		menus.each(function () {
			if ($(this).hasClass("target")) {

			}
			else {
				$(this).removeClass("selected");
				$(this).children().removeClass("selected");
			}
		});

		if ($(this).hasClass("selected")) {
			$(this).removeClass("selected");
			$(this).children().removeClass("selected");
		}
		else {
			$(this).addClass("selected");
			$(this).children().addClass("selected");
		}
		$('.mobile-item.dropdown').removeClass("target");
	});

	$('a.bullet.sub').click(function (event) {
		event.stopImmediatePropagation();
		console.log("Clicked submenu bullet...");
		if ($(this).hasClass("on")) {
			$(this).removeClass("on");
			$(this).parent().find('.dropdown-submenu').css("display", "none");
		}
		else {
			$(this).addClass("on");
			$(this).parent().find('.dropdown-submenu').css("display", "block");
		}
	});

	$('.progress-report > p').click(function () {
		var iframe = $(this).parent().find('iframe');
		var text = $(this);
		if (text.hasClass('no-padding')) {
			text.removeClass('no-padding');
			iframe.removeClass('display-none');
		}
		else {
			text.addClass('no-padding');
			iframe.addClass('display-none');
		}
	});
});

//-------------------------------------------------------------------------------------------------
function displayMenu() {
	$('#mobileMenu').css("display", "block");
	$('#mobileMenuButton').html("&#9746;");
	$('#mobileMenuButton').removeClass("menu-label");
	$('#mobileMenuButton').addClass("close-label");
	$('#mobileMenuButton').removeClass("off");
	$('#mobileMenuButton').addClass("on");
	$('.banner-section .content').addClass("invisible");
	$('.mobile-overlay').addClass("visible");
}

function hideMenu() {
	$('.mobile-overlay').removeClass("visible");
	$('#mobileMenuButton').removeClass("on");
	$('#mobileMenuButton').addClass("off");
	$('#mobileMenuButton').html("&#9776;");
	$('.banner-section .content').removeClass("invisible");
	$('#mobileMenuButton').addClass("menu-label");
	$('#mobileMenuButton').removeClass("close-label");
	$('#mobileMenu').css("display", "none");
	$('.mobile-item.dropdown').removeClass("selected");
	$('.mobile-item.dropdown').children().removeClass("selected");
	$('a.bullet.sub').removeClass("on");
	$('a.bullet.sub').parent().find('.dropdown-submenu').css("display", "none");
}

function matchCard1Heights() {
	$('.card1').matchHeight();
	if ($(window).width() > 991) {
		$('.card1 .title').matchHeight();
		$('.card1 .main-block').matchHeight();
	}
	else {
		var maxHeight = 0;
		var card1s = $('.card1-section').children();
		card1s.each(function () {
			console.log($(this).find('.blurb').height());
			if ($(this).find('.blurb').height() > maxHeight) {
				maxHeight = $(this).find('.blurb').height();
			}
		});
		if (maxHeight > 100) { // Change this if you need more text in the blurb...It's just an upper bound
			maxHeight = 100;
		}
		console.log(maxHeight);
		$('.card1 .blurb').css('min-height', maxHeight + 'px');
		$('.card1 a').css('bottom', maxHeight - 35 + 'px');
	}
}

function bindCarousel() {
	console.log('bindCarousel()');
	var indicators = $('#android-carousel').find('.carousel-indicators');
	var active = indicators.find('.active');
	//console.log('active', active);
	for (var i = 1; i < indicators.children().length + 1; i++) {
		var target = 'ind' + i;
		//console.log(target);
		if (active.hasClass(target)) {
			//console.log(active, "has this class => ", target);
			contentRows = $('#android-carousel').parent().parent().parent().find('.right-column').children();
			for (var j = 0; j < contentRows.length; j++) {
				var indicator = contentRows.eq(j).find('.number-wrapper');
				if (indicator.hasClass(target)) {
					if (!indicator.hasClass('active')) {
						indicator.addClass('active');
					}
				}
				else {
					if (indicator.hasClass('active')) {
						indicator.removeClass('active');
					}
				}
			}
			break;
		}
	}
	setTimeout(bindCarousel, 200);
}