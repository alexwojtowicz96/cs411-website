<?php 
	/**
	* Given a header section and css directory, return the <head> contents
	*/
	function get_header_section($head, $cssDir) {
		return str_replace("?CSSDIR", $cssDir, $head);
	}

	/**
	* Given a script section and js directory, return the <script> contents
	*/
	function get_script_section($script, $jsDir) {
		return str_replace("?JSDIR", $jsDir, $script);
	}

	/**
	* Given a section and image directory, update the references to images and return the contents
	*/ 
	function get_section_with_images($section, $imgDir) {
		return str_replace("?IMGDIR", $imgDir, $section);
	}

	/**
	* Picks a random number between 1 and the number of files in the img/banners/ directory
	*/
	function pick_random_num_for_banner($imgDir) {
		$directory = $imgDir . "/banners/";
		$filecount = 0;
		$files = glob($directory . "*");
		if ($files) { 
 			$filecount = count($files);
		}
		$bannerNum = mt_rand(1, $filecount);

		return $bannerNum;
	}

	/**
	* Given a banner section, image directory, and some content, return the banner section.
	* This banner will be randomly selected, but the content and styles are static.
	*/
	function get_banner_section($banner, $imgDir, $content) {
		$bannerChoice = pick_random_num_for_banner($imgDir);
		$banner = get_section_with_images($banner, $imgDir);
		$banner = str_replace("?BANNERNUM", $bannerChoice, $banner);
		$banner = str_replace("?BANNERCONTENT", $content, $banner);

		return $banner;
	}

	/**
	* To get the nav to link up properly and render properly, provide a string with the nav partial and
	* the relative paths of the php and image directories from the current file.
	*/
	function get_nav_section($nav, $phpDir, $imgDir) {
		$nav = get_section_with_images($nav, $imgDir);

		return str_replace("?PHPDIR", $phpDir, $nav);
	}

	/**
	* Renders a card1 section with the array of card data (of length 3). 
	*
	* $cards => Array of length 3 with card data (keyAlias => value)
	* $phpDir => /php directory
	* $imgDir => /img directory
	* $card1 => The content found in /php/partials/card1.php
	*/
	function render_card1_section($cards, $phpDir, $imgDir, $card1) {
		$section = $card1;
		foreach ($cards as $contentKey => $contentVal) {
			$index = $contentKey + 1;
			foreach ($contentVal as $attrKey => $attrVal) {
				$alias = strtoupper($attrKey);
				$variable = "?" . $alias . $index;
				$section = str_replace($variable, $attrVal, $section); 
			}
		}

		return $section;
	}

	/**
	* Renders the android carousel with the supplied content. This content allows for
	* 5 steps to be supplied, meaning that if $content.steps != $content.images != 5,
	* there is a problem!
	* 
	* The contents of $content should be:
	*	[0] $heading
	*	[1] $subheading
	*	[2] $steps (length == 5)
	*	[3] $images (length == 5)
	*/
	function render_android_carousel_section($content, $phpDir, $imgDir, $android_carousel) {
		$section = get_section_with_images($android_carousel, $imgDir);
		$section = str_replace("?HEADING", $content[0], $section);
		$section = str_replace("?SUBHEADING", $content[1], $section);
		foreach ($content[2] as $stepKey => $stepVal) {
			$variable = "?" . strtoupper($stepKey);
			$section = str_replace($variable, $stepVal, $section);
		}
		foreach ($content[3] as $imgKey => $imgVal) {
			$variable = "?" . strtoupper($imgKey);
			$section = str_replace($variable, $imgVal, $section);
		}

		return $section;
	}

	/**
	* Given the content and the markup, render this presentation section...
	* $content = [
	* 	[0] => $title
	*	[1] => $description
	*	[2] => $iframe
	*	[3] => $icon
	*	[4] => $pdf
	* ];
	*/
	function render_presentation_section($content, $presentation_section) {
		$section = $presentation_section;
		foreach ($content as $alias => $value) {
			$variable = "?" . strtoupper($alias);
			$section = str_replace($variable, $value, $section);
		}

		return $section;
	}

	/**
	* Given an array of paths to pdfs, fill these in for the corresponding group
	* with the given team lab section content.
	*/
	function render_team_lab_section($lab_paths, $icon, $team_lab_section) {
		$section = $team_lab_section;
		foreach ($lab_paths as $alias => $value) {
			$variable = "?" . strtoupper($alias);
			$section = str_replace($variable, $value, $section);
		}
		$section = str_replace("?ICON", $icon, $section);

		return $section;
	}
?>