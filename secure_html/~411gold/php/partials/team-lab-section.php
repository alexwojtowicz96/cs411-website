<section class='team-lab-section'>
	<div class='row'>
		<div class='card-wrapper'>
			<div class='team-lab link-enabled'>
				<a href='?ALEXLAB'></a>
				<p class='who'>Alex Wojtowicz</p>
				<div>
					<img src='?ICON' />
				</div>
				<p class='msg'>Click To Download</p>
			</div>
		</div>
		<div class='card-wrapper'>
			<div class='team-lab link-enabled'>
				<a href='?BRANDONLAB'></a>
				<p class='who'>Brandon Campbell</p>
				<div>
					<img src='?ICON' />
				</div>
				<p class='msg'>Click To Download</p>
			</div>
		</div>
		<div class='card-wrapper'>
			<div class='team-lab link-enabled'>
				<a href='?DUNCANLAB'></a>
				<p class='who'>Duncan Holterhaus</p>
				<div>
					<img src='?ICON' />
				</div>
				<p class='msg'>Click To Download</p>
			</div>
		</div>
	</div>
	<div class='row'>
		<div class='card-wrapper'>
			<div class='team-lab link-enabled'>
				<a href='?DWIGHTLAB'></a>
				<p class='who'>Dwight Owings</p>
				<div>
					<img src='?ICON' />
				</div>
				<p class='msg'>Click To Download</p>
			</div>
		</div>
		<div class='card-wrapper'>
			<div class='team-lab link-enabled'>
				<a href='?JOHNLAB'></a>
				<p class='who'>John Hessefort</p>
				<div>
					<img src='?ICON' />
				</div>
				<p class='msg'>Click To Download</p>
			</div>
		</div>
		<div class='card-wrapper'>
			<div class='team-lab link-enabled'>
				<a href='?EDWINLAB'></a>
				<p class='who'>Edwin Ordona</p>
				<div>
					<img src='?ICON' />
				</div>
				<p class='msg'>Click To Download</p>
			</div>
		</div>
	</div>
</section>
