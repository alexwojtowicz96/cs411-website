<footer class='footer-wrapper'>
	<div class='footer-main container'>
		<div class='left'>
			<!-- <img src='?IMGDIR/nav-logo.png' alt='tutor dash logo' /> -->
			<p class='tutordash'>Tutor Dash</p>
			<p>All Rights Reserved</p>
			<p> CS 411W <span class="">Team Gold</span></p>
			<div class='rights'>
				<p>&copy;2019 ODU</p>
				<img src='?IMGDIR/odu-crown.png' alt='odu crown logo' />
			</div>
		</div>
		<div class='center'>
			<div class='img-group'>
				<!-- <p>Find a tutor. Be a tutor.</p> -->
				<img class='screen' src='?IMGDIR/phone-landscape.png' alt='iphone android landscape' />
				<img class='screen-fill' src='?IMGDIR/icons/android.png' alt='android' />
			</div>
		</div>
		<div class='right'>
			<p>Old Dominion University</p>
			<p>Norfolk VA, 23529</p>
			<P>Department of Computer Science</P>
			<a href='https://www.odu.edu/compsci' target='_blank'>https://www.odu.edu/compsci/</a>
		</div>
	</div>
</footer>