<!--
	This carousel design allows for 5 features, so keep that in mind...
-->

<div class="android-carousel-section container">
	<div class='header'>
		<p class='heading'>?HEADING</p>
		<p class='subheading'>?SUBHEADING</p>
	</div>
	<div class='carousel-content'>
		<div class='left-column'>
			<img class='cutout' src='?IMGDIR/phone-cutout.png' alt='android phone device' />
			<div class='screen-constrain'>
				<div id="android-carousel" class="carousel slide" data-ride='carousel'>
					<ol class='carousel-indicators'>
						<li data-target='#android-carousel' data-slide-to='0' class='active ind1'></li>
						<li data-target='#android-carousel' data-slide-to='1' class='ind2'></li>
						<li data-target='#android-carousel' data-slide-to='2' class='ind3'></li>
						<li data-target='#android-carousel' data-slide-to='3' class='ind4'></li>
						<li data-target='#android-carousel' data-slide-to='4' class='ind5'></li>
					</ol>
					<div class='carousel-inner'>
						<div class='carousel-item active'>
							<img src='?IMG1' alt='phone screenshot' />
						</div>
						<div class='carousel-item'>
							<img src='?IMG2' alt='phone screenshot' />
						</div>
						<div class='carousel-item'>
							<img src='?IMG3' alt='phone screenshot' />
						</div>
						<div class='carousel-item'>
							<img src='?IMG4' alt='phone screenshot' />
						</div>
						<div class='carousel-item'>
							<img src='?IMG5' alt='phone screenshot' />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class='right-column'>
			<div class='content-row'>
				<div class='number-wrapper ind1' data-target='#android-carousel' data-slide-to='0'>
					<p>1</p>
				</div>
				<div class='text-wrapper'>
					<p>?STEP1</p>
				</div>
			</div>
			<div class='content-row'>
				<div class='number-wrapper ind2' data-target='#android-carousel' data-slide-to='1'>
					<p>2</p>
				</div>
				<div class='text-wrapper'>
					<p>?STEP2</p>
				</div>
			</div>
			<div class='content-row'>
				<div class='number-wrapper ind3' data-target='#android-carousel' data-slide-to='2'>
					<p>3</p>
				</div>
				<div class='text-wrapper'>
					<p>?STEP3</p>
				</div>
			</div>
			<div class='content-row'>
				<div class='number-wrapper ind4' data-target='#android-carousel' data-slide-to='3'>
					<p>4</p>
				</div>
				<div class='text-wrapper'>
					<p>?STEP4</p>
				</div>
			</div>
			<div class='content-row'>
				<div class='number-wrapper ind5' data-target='#android-carousel' data-slide-to='4'>
					<p>5</p>
				</div>
				<div class='text-wrapper'>
					<p>?STEP5</p>
				</div>
			</div>
		</div>
	</div>
</div>