<!-- 
	IMPORTANT! - If you are updating the nav, be sure to do this both in the mobile navigation
	AND primary navigation sections!!!

	Use ?IMGDIR as the image directory placeholder.
	Use ?PHPDIR as the php directory placeholder.
-->

<!-- Utility Navigation ..................................................................................... -->
<nav class='utility-navigation'>
	<nav class='navbar navbar-expand-sm container'>
		<div class='navbar-wrapper'>
			<ul class='navbar-nav'>
				<li class='nav-item'>
					<a class='nav-link' href='?PHPDIR/pages/faq.php'>FAQ</a>	
				</li>
				<li class='nav-item'>
					<a class='nav-link' href='?PHPDIR/pages/contact-us.php'>Contact Us</a>
				</li>
			</ul>
		</div>
	</nav>
</nav>

<!-- Mobile Navigation ...................................................................................... -->
<nav class='mobile-navigation'>
	<nav class='navbar navbar-expand-sm'>
		<a class='logo-wrapper' href='?PHPDIR/../index.php'>
			<img class='nav-logo' src='?IMGDIR/nav-logo.png' alt='tutor dash logo' />
		</a>
		<span class='menu-btn menu-label off' id='mobileMenuButton'>&#9776;</span>
	</nav>
	<nav class='mobile-menu' id='mobileMenu'>
		<ul class='mobile-nav'>
			<li class='mobile-item dropdown'>
				<a class='mobile-link'>About Us</a>
				<ul class='mobile-dropdown-menu'>
					<li>
						<a class='bullet' href='?PHPDIR/pages/about-us/what-is-tutor-dash.php'>What Is Tutor Dash?</a>
					</li>
					<li>
						<a class='bullet' href='?PHPDIR/pages/about-us/our-team.php'>Our Team</a>
					</li>
					<li>
						<a class='bullet' href='?PHPDIR/pages/about-us/the-problem.php'>The Problem</a>
					</li>
					<li>
						<a class='bullet' href='?PHPDIR/pages/about-us/our-solution.php'>Our Solution</a>
					</li>
				</ul>
			</li>
			<li class='mobile-item dropdown'>
				<a class='mobile-link'>Presentations</a>
				<ul class='mobile-dropdown-menu'>
					<li>
						<a class='bullet' href='?PHPDIR/pages/presentations/the-concept.php'>The Concept</a>
					</li>
					<li class='dropdown'>
						<a class='bullet sub'>Feasibility</a>
						<ul class='dropdown-submenu'>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/presentations/feasibility/version-1.php'>Version 1.0</a>
							</li>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/presentations/feasibility/version-2.php'>Version 2.0</a>
							</li>
						</ul>
					</li>
					<li class='dropdown'>
						<a class='bullet sub'>Design</a>
						<ul class='dropdown-submenu'>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/presentations/design/version-1.php'>Version 1.0</a>
							</li>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/presentations/design/version-2.php'>Version 2.0</a>
							</li>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/presentations/design/prototype.php'>Prototype</a>
							</li>
						</ul>
					</li>
					<li class='dropdown'>
						<a class='bullet sub'>Demonstrations</a>
						<ul class='dropdown-submenu'>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/presentations/demonstrations/elevator-pitch.php'>Elevator Pitch (Demo 1)</a>
							</li>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/presentations/demonstrations/demonstration-2.php'>Demonstration 2</a>
							</li>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/presentations/demonstrations/demonstration-3.php'>Demonstration 3</a>
							</li>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/presentations/demonstrations/demonstration-4.php'>Demonstration 4</a>
							</li>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/presentations/demonstrations/final-demo.php'>Final Demo (Demonstration 5)</a>
							</li>
						</ul>
					</li>
				</ul>
			</li>
			<li class='mobile-item dropdown'>
				<a class='mobile-link'>Deliverables</a>
				<ul class='mobile-dropdown-menu'>
					<li class='dropdown'>
						<a class='bullet sub'>Process Flows</a>
						<ul class='dropdown-submenu'>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/deliverables/process-flows/current-process.php'>Current Process</a>
							</li>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/deliverables/process-flows/solution.php'>Solution</a>
							</li>
						</ul>
					</li>
					<li class='dropdown'>
						<a class='bullet sub'>Diagrams</a>
						<ul class='dropdown-submenu'>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/deliverables/diagrams/mfcd.php'>Major Functional Components</a>
							</li>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/deliverables/diagrams/wbs.php'>Work Breakdown Structure</a>
							</li>
						</ul>
					</li>
					<li class='dropdown'>
						<a class='bullet sub'>Matrices</a>
						<ul class='dropdown-submenu'>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/deliverables/matrices/competition.php'>Competition</a>
							</li>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/deliverables/matrices/risks.php'>Risks</a>
							</li>
						</ul>
					</li>
					<li class='dropdown'>
						<a class='bullet sub'>Algorithms</a>
						<ul class='dropdown-submenu'>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/deliverables/algorithms/pdf-transcript-parser.php'>PDF Transcript Parser</a>
							</li>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/deliverables/algorithms/pay-rate-calculator.php'>Pay-Rate Calculator</a>
							</li>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/deliverables/algorithms/relative-distance-estimator.php'>Relative Distance Estimator</a>
							</li>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/deliverables/algorithms/web-conference-creator.php'>Web Conference Creator</a>
							</li>
							<li class='link-enabled'>
								<a class='bullet' href='?PHPDIR/pages/deliverables/algorithms/payment-logic.php'>Payment Logic</a>
							</li>
						</ul>
					</li>
				</ul>
			</li>
			<li class='mobile-item link-enabled'>
				<a class='mobile-link' href='?PHPDIR/pages/user-stories.php'>User Stories</a>
			</li>
			<li class='mobile-item dropdown'>
				<a class='mobile-link'>Labs</a>
				<ul class='mobile-dropdown-menu'>
					<li>
						<a class='bullet' href='?PHPDIR/pages/lab-reports/lab-1.php'>Lab I</a>
					</li>
					<li>
						<a class='bullet' href='?PHPDIR/pages/lab-reports/lab-2.php'>Lab II</a>
					</li>
					<li>
						<a class='bullet' href='?PHPDIR/pages/lab-reports/lab-3.php'>Lab III</a>
					</li>
					<li>
						<a class='bullet' href='?PHPDIR/pages/lab-reports/lab-4.php'>Lab IV</a>
					</li>
				</ul>
			</li>
			<li class='mobile-item dropdown'>
				<a class='mobile-link'>Development</a>
				<ul class='mobile-dropdown-menu'>
					<li>
						<a class='bullet' href='https://git-community.cs.odu.edu/bcampbe/cs411-gold' target='_blank'>Repository</a>
					</li>
					<li>
						<a class='bullet' href='https://trello.com/b/sZngmD1w/tutor-dash-prototype' target='_blank'>Trello Board</a>
					</li>
					<li>
						<a class='bullet' href='?PHPDIR/pages/development/schedule.php'>Schedule</a>
					</li>
					<li>
						<a class='bullet' href='?PHPDIR/pages/development/progress-reports.php'>Weekly Progress Reports</a>
					</li>
					<li>
						<a class='bullet' href='?PHPDIR/pages/development/download-app.php'>Download ver.1.0</a>
					</li>
				</ul>
			</li>
			<li class='mobile-item link-enabled'>
				<a class='mobile-link' href='?PHPDIR/pages/references.php'>References</a>
			</li>
			<li class='mobile-item link-enabled'>
				<a class='mobile-link' href='?PHPDIR/pages/faq.php'>FAQ</a>
			</li>
			<li class='mobile-item link-enabled'>
				<a class='mobile-link' href='?PHPDIR/pages/contact-us.php'>Contact Us</a>
			</li>
		</ul>
	</nav>
	<div class='mobile-overlay'></div>
</nav>

<!-- Primary Navigation ..................................................................................... -->
<nav class='primary-navigation'>
	<nav class='navbar navbar-expand-sm container'>
		<a class='logo-wrapper' href='?PHPDIR/../index.php'>
			<img class='nav-logo' src='?IMGDIR/nav-logo.png' alt='tutor dash logo' />
		</a>
		<div class='navbar-wrapper'>
			<ul class='navbar-nav'>
				<li class='nav-item dropdown'>
					<a class='nav-link'>About Us</a>
					<ul class='dropdown-menu'>
						<li class='link-enabled'>
							<a href='?PHPDIR/pages/about-us/what-is-tutor-dash.php'>What Is Tutor Dash?</a>
						</li>
						<li class='link-enabled'>
							<a href='?PHPDIR/pages/about-us/our-team.php'>Our Team</a>
						</li>
						<li class='link-enabled'>
							<a href='?PHPDIR/pages/about-us/the-problem.php'>The Problem</a>
						</li>
						<li class='link-enabled'>
							<a href='?PHPDIR/pages/about-us/our-solution.php'>Our Solution</a>
						</li>
					</ul>
				</li>
				<li class='nav-item dropdown'>
					<a class='nav-link'>Presentations</a>
					<ul class='dropdown-menu'>
						<li class='link-enabled'>
							<a href='?PHPDIR/pages/presentations/the-concept.php'>The Concept</a>
						</li>
						<li class='dropdown dropdown-submenu'>
							<a>Feasibility</a>
							<ul class='dropdown-menu'>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/presentations/feasibility/version-1.php'>Version 1.0</a>
								</li>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/presentations/feasibility/version-2.php'>Version 2.0</a>
								</li>
							</ul>
						</li>
						<li class='dropdown dropdown-submenu'>
							<a>Design</a>
							<ul class='dropdown-menu'>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/presentations/design/version-1.php'>Version 1.0</a>
								</li>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/presentations/design/version-2.php'>Version 2.0</a>
								</li>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/presentations/design/prototype.php'>Prototype</a>
								</li>
							</ul>
						</li>
						<li class='dropdown dropdown-submenu'>
							<a>Demonstrations</a>
							<ul class='dropdown-menu'>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/presentations/demonstrations/elevator-pitch.php'>Elevator Pitch (Demo 1)</a>
								</li>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/presentations/demonstrations/demonstration-2.php'>Demonstration 2</a>
								</li>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/presentations/demonstrations/demonstration-3.php'>Demonstration 3</a>
								</li>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/presentations/demonstrations/demonstration-4.php'>Demonstration 4</a>
								</li>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/presentations/demonstrations/final-demo.php'>Final Demo (Demonstration 5)</a>
								</li>
							</ul>
						</li>
					</ul>
				</li>
				<li class='nav-item dropdown'>
					<a class='nav-link'>Deliverables</a>
					<ul class='dropdown-menu'>
						<li class='dropdown dropdown-submenu'>
							<a>Process Flows</a>
							<ul class='dropdown-menu'>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/deliverables/process-flows/current-process.php'>Current Process</a>
								</li>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/deliverables/process-flows/solution.php'>Solution</a>
								</li>
							</ul>
						</li>
						<li class='dropdown dropdown-submenu'>
							<a>Diagrams</a>
							<ul class='dropdown-menu'>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/deliverables/diagrams/mfcd.php'>Major Functional Components</a>
								</li>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/deliverables/diagrams/wbs.php'>Work Breakdown Structure</a>
								</li>
							</ul>
						</li>
						<li class='dropdown dropdown-submenu'>
							<a>Matrices</a>
							<ul class='dropdown-menu'>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/deliverables/matrices/competition.php'>Competition</a>
								</li>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/deliverables/matrices/risks.php'>Risks</a>
								</li>
							</ul>
						</li>
						<li class='dropdown dropdown-submenu'>
							<a>Algorithms</a>
							<ul class='dropdown-menu'>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/deliverables/algorithms/pdf-transcript-parser.php'>PDF Transcript Parser</a>
								</li>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/deliverables/algorithms/pay-rate-calculator.php'>Pay-Rate Calculator</a>
								</li>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/deliverables/algorithms/relative-distance-estimator.php'>Relative Distance Estimator</a>
								</li>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/deliverables/algorithms/web-conference-creator.php'>Web Conference Creator</a>
								</li>
								<li class='link-enabled'>
									<a href='?PHPDIR/pages/deliverables/algorithms/payment-logic.php'>Payment Logic</a>
								</li>
							</ul>
						</li>
					</ul>
				</li>
				<li class='nav-item link-enabled'>
					<a class='nav-link' href='?PHPDIR/pages/user-stories.php'>User Stories</a>
				</li>
				<li class='nav-item dropdown'>
					<a class='nav-link'>Labs</a>
					<ul class='dropdown-menu'>
						<li class='link-enabled'>
							<a href='?PHPDIR/pages/lab-reports/lab-1.php'>Lab I</a>
						</li>
						<li class='link-enabled'>
							<a href='?PHPDIR/pages/lab-reports/lab-2.php'>Lab II</a>
						</li>
						<li class='link-enabled'>
							<a href='?PHPDIR/pages/lab-reports/lab-3.php'>Lab III</a>
						</li>
						<li class='link-enabled'>
							<a href='?PHPDIR/pages/lab-reports/lab-4.php'>Lab IV</a>
						</li>
					</ul>
				</li>
				<li class='nav-item dropdown'>
					<a class='nav-link'>Development</a>
					<ul class='dropdown-menu'>
						<li class='link-enabled new-tab'>
							<a href='https://git-community.cs.odu.edu/bcampbe/cs411-gold' target='_blank'>Repository</a>
						</li>
						<li class='link-enabled new-tab'>
							<a href='https://trello.com/b/sZngmD1w/tutor-dash-prototype' target='_blank'>Trello Board</a>
						</li>
						<li class='link-enabled'>
							<a href='?PHPDIR/pages/development/schedule.php'>Schedule</a>
						</li>
						<li class='link-enabled'>
							<a href='?PHPDIR/pages/development/progress-reports.php'>Weekly Progress Reports</a>
						</li>
						<li class='link-enabled'>
							<a href='?PHPDIR/pages/development/download-app.php'>Download ver.1.0</a>
						</li>
					</ul>
				</li>
				<li class='nav-item link-enabled'>
					<a class='nav-link' href='?PHPDIR/pages/references.php'>References</a>
				</li>
			</ul>
		</div>
	</nav>
</nav>