<!--
	Use this to render a presentation section. Variabes are:
		?TITLE
		?DESCRIPTION
		?IFRAME
		?ICON
		?PDF
-->

<article>
	<div class='article'>
		<div class='presentation-wrapper'>
			<iframe src="?IFRAME" frameborder="0" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
		</div>
	</div>
	<div class='sidebar'>
		<p><span class='gold wide'>?TITLE</span></p>
		<p>?DESCRIPTION</p>
		<div class='img-wrapper'>
			<img src="?ICON" alt='idea' />
		</div>
		<div class='download'>
			<a href="?PDF" target="_blank">Download The PDF</a>
		</div>
	</div>
</article>