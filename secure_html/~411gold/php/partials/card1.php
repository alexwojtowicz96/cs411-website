<!-- 
	This is a card1-section. It must be rendered in this three-column row configuration
-->

<section class="three-columns card1-section">
	<div class='card1 link-enabled flyin'>
		<div class='main-block'>
			<p class='title'>?TITLE1</p>
			<div class='img-wrapper'>
				<img class='img' src='?IMAGE1' alt='technology about' />
			</div>
			<p class='blurb'>?BLURB1</p>
		</div>
		<div class="btn-wrapper">
			<a href='?LINK1'>Learn More</a>
		</div>
	</div>
	<div class='card1 link-enabled flyin delay2'>
		<div class='main-block'>
			<p class='title'>?TITLE2</p>
			<div class='img-wrapper'>
				<img class='img' src='?IMAGE2' alt='technology about' />
			</div>
			<p class='blurb'>?BLURB2</p>
		</div>
		<div class="btn-wrapper">
			<a href='?LINK2'>Learn More</a>
		</div>
	</div>
	<div class='card1 link-enabled flyin delay3'>
		<div class='main-block'>
			<p class='title'>?TITLE3</p>
			<div class='img-wrapper'>
				<img class='img' src='?IMAGE3' alt='technology about' />
			</div>
			<p class='blurb'>?BLURB3</p>
		</div>
		<div class="btn-wrapper">
			<a href='?LINK3'>Learn More</a>
		</div>
	</div>
</section>