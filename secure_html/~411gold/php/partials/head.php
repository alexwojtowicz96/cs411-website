<!-- 
	Add shared stylesheets here (just put ?CSSDIR as the css directory) 
-->

<meta name='viewport' content='width=device-width, initial-scale=1' />
<link rel='shortcut icon' type='image/png' href='?CSSDIR/../img/favicon/favicon.png' />
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,900|Work+Sans:400,600|Montserrat:300,400,600,700,900' />
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' />
<link rel='stylesheet' href='?CSSDIR/_layout.css' />
<link rel='stylesheet' href='?CSSDIR/_variables.css'>
<link rel='stylesheet' href='?CSSDIR/layouts/_homepage.css' />
<link rel='stylesheet' href='?CSSDIR/layouts/_primary-navigation.css' />
<link rel='stylesheet' href='?CSSDIR/layouts/_mobile-navigation.css' />
<link rel='stylesheet' href='?CSSDIR/layouts/_utility-navigation.css' />
<link rel='stylesheet' href='?CSSDIR/layouts/_footer.css' />
<link rel='stylesheet' href='?CSSDIR/layouts/_banner.css' />
<link rel='stylesheet' href='?CSSDIR/cards/_card1.css' />
<link rel='stylesheet' href='?CSSDIR/features/_android-carousel.css' />
<link rel='stylesheet' href='?CSSDIR/layouts/_subpage.css' />
<link rel='stylesheet' href='?CSSDIR/layouts/_team-section.css' />
<link rel='stylesheet' href='?CSSDIR/layouts/_concept-container.css' />
<link rel='stylesheet' href='?CSSDIR/layouts/_presentation.css' />
<link rel='stylesheet' href='?CSSDIR/layouts/_table.css' />
<link rel='stylesheet' href='?CSSDIR/cards/_risks-card.css' />
<link rel='stylesheet' href='?CSSDIR/cards/_role-card.css' />
<link rel='stylesheet' href='?CSSDIR/forms/_contact-form.css' />
<link rel='stylesheet' href='?CSSDIR/layouts/_references.css' />
<link rel='stylesheet' href='?CSSDIR/layouts/_lab-report.css' />
<link rel='stylesheet' href='?CSSDIR/cards/_team-lab.css' />