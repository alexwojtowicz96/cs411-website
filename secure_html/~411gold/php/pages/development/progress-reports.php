<?php
	$cssDir = "../../../css";  // relative path of css directory
	$jsDir = "../../../js";    // relative path of js directory
	$imgDir = "../../../img";  // relative path of img directory
	$phpDir = "../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
?>
<!DOCTYPE html>
<html class="subpage"> <!-- Make sure you append any specific page styles to this class name if styles on that page need overriding -->
	<title>Tutor Dash | Progress Reports</title> <!-- Update this with the page title -->
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "Progress Reports";  // add banner text here
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>Progress Reports</p>
						<p class='subheading'>You will find that our progress truely begins on week 2. During week 1, we were still gathering our resources, so the first actual developement progress report begins on week 2 even though it is labeled as the first one.</p>
					</div>
					<div class='progress-report'>
						<p class='no-padding'>(1) Progress Report For Week #2</p>
						<iframe class='display-none' src="https://docs.google.com/document/d/e/2PACX-1vTJ7j-ti3b4dLuC9oDip1rpEmwcju7f9UsFydV0BAjDlt-pJVZDhPKBO3DccCHMrqld64T9hv5Od3hl/pub?embedded=true"></iframe>
					</div>
					<div class='progress-report'>
						<p class='no-padding'>(2) Progress Report For Week #3</p>
						<iframe class='display-none' src="https://docs.google.com/document/d/e/2PACX-1vShIz6FS461iRlTVrhkxjugkKUcmLT_87Otm-okhlPxF7xHei2thZEyRn5Mlp95NU4jOv4z0CXW3b6r/pub?embedded=true"></iframe>
					</div>
					<div class='progress-report'>
						<p class='no-padding'>(3) Progress Report For Week #4</p>
						<iframe class='display-none' src="https://docs.google.com/document/d/e/2PACX-1vTx_kv6afy-EGsvu3_lEKzmVMZn4Y3uRnd27TrMuUbgp7k82V-rLReZRMOIZhoT-Zk3oBF0Qke0zsR4/pub?embedded=true"></iframe>
					</div>
					<div class='progress-report'>
						<p class='no-padding'>(4) Progress Report For Week #5</p>
						<iframe class='display-none' src="https://docs.google.com/document/d/e/2PACX-1vQhfFb6--H0ZUz1wzpp9QpyaElWxx8VZBbTvt5KpKVQ3dDBvg2cgpWJw-QuJHtUXeHPIh9c2ZTGh5PS/pub?embedded=true"></iframe>
					</div>
					<div class='progress-report'>
						<p class='no-padding'>(5) Progress Report For Week #6</p>
						<iframe class='display-none' src="https://docs.google.com/document/d/e/2PACX-1vSZDmLFPQ7yJjjNTTLx1qWfiqOa9BqxKXK-iotW9KEwO3at_8NH5nSsYNmMo8cHxSjjebokH95psZUf/pub?embedded=true"></iframe>
					</div>
					<div class='progress-report'>
						<p class='no-padding'>(6) Progress Report For Week #7</p>
						<iframe class='display-none' src="https://docs.google.com/document/d/e/2PACX-1vSP0Q6MS3g36lnhkIuIvYceJuvIEPDWC-8JD5mtwP7BzNNXWs96mitrirFEZCx4fSoq57sVnhaaTqDq/pub?embedded=true"></iframe>
					</div>
					<div class='progress-report'>
						<p class='no-padding'>(7) Progress Report For Week #8</p>
						<iframe class='display-none' src="https://docs.google.com/document/d/e/2PACX-1vR3sMsh9SPTeBpbDmhnRyMXCagJw8zwHZdxqI7FXcL9IHVA-WPKw6UkyuaArPhzLiLj1TWFozZ7r7sl/pub?embedded=true"></iframe>
					</div>
					<div class='progress-report'>
						<p class='no-padding'>(8) Progress Report For Week #9</p>
						<iframe class='display-none' src="https://docs.google.com/document/d/e/2PACX-1vSMracBdbFSnQIHS-p1wvHwtWDOcyRRTlhK3mlo8ZZ3jaKI-XIl2dpldbmEXFaikpcglcqk3NFh2oBC/pub?embedded=true"></iframe>
					</div>
					<div class='progress-report'>
						<p class='no-padding'>(9) Progress Report For Week #10</p>
						<iframe class='display-none' src="https://docs.google.com/document/d/e/2PACX-1vQ8U37FNqiXb5QFzugqin4doWp-WOdQWwTjWhV8JO3viuqVSwReBnqoJbu0VVw-LBjp3d7nrnN4F8gn/pub?embedded=true"></iframe>
					</div>
					<div class='progress-report'>
						<p class='no-padding'>(10) Progress Report For Week #11</p>
						<iframe class='display-none' src="https://docs.google.com/document/d/e/2PACX-1vTXj8j7dtzHPQ6PZSPuwMyvKaiLRae8-AFGmD3v2gUrPxN-J2bojjoPh8G1NgKgV8OKN9bbyiNEMjjz/pub?embedded=true"></iframe>
					</div>
					<div class='progress-report'>
						<p class='no-padding'>(11) Progress Report For Week #12</p>
						<iframe class='display-none' src="https://docs.google.com/document/d/e/2PACX-1vRXbQ9OQ3EkSeCOKoMygHk5yTKM14LuaP976MnYcL6HS8OuIWw_wiIBfcdxXfN5xlMeVfcv3iMeWpVd/pub?embedded=true"></iframe>
					</div>
					<div class='progress-report'>
						<p class='no-padding'>(12) Progress Report For Week #13</p>
						<iframe class='display-none' src="https://docs.google.com/document/d/e/2PACX-1vQKTRL2ZT13CSpp6_MumqBr7BQYLmqxkaAWtxksWCh697FPdEJbCVn90px5b_2WCWIDh6BUy4CDWs82/pub?embedded=true"></iframe>
					</div>
					<div class='progress-report'>
						<p class='no-padding'>(13) Progress Report For Week #14</p>
						<iframe class='display-none' src="https://docs.google.com/document/d/e/2PACX-1vS6uLxTTob_hM6hL8ItIZAZY5jqbSo-YbXDkXMzt6evLYEEv25qwfG1ViGLr7G_QDZ7-5Q8xMgrAQvr/pub?embedded=true"></iframe>
					</div>
					<div class='progress-report'>
						<p class='no-padding'>(14) Progress Report For Week #15</p>
						<iframe class='display-none' src="https://docs.google.com/document/d/e/2PACX-1vSh_VctAaeexlfN-9WPkGs_etUmtVyIzYaGccxcfqfrXqXCo73XxJcds6B8YFI9MN--70iVArU1bK6r/pub?embedded=true"></iframe>
					</div>
					<br />

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>