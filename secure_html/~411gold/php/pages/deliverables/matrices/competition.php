<?php
	$cssDir = "../../../../css";  // relative path of css directory
	$jsDir = "../../../../js";    // relative path of js directory
	$imgDir = "../../../../img";  // relative path of img directory
	$phpDir = "../../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
?>
<!DOCTYPE html>
<html class="subpage scale-sidebar"> 
	<title>Tutor Dash | Competition</title> 
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "Our Competition";
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class="header">
						<p class='heading'>Analyzing Our Competitors</p>
					</div>
					<div class="gradient1 article-has-bg">
						<article>
							<div class="article">
								<table class='competition'>
									<!--
										&#9745; => Check
										&#9746; => X
									-->
									<tr>
										<th>Features</th>
										<th class='green'>Tutor Dash</th>
										<th class='red'>Tutor.com</th>
										<th class='red'>Tutor Matching Service</th>
										<th class='red'>Skooli</th>
										<th class='red'>Wyzant</th>
										<th class='red'>HeyTutor</th>
										<th class='orange'>Care.com</th>
										<th class='orange'>Public Facebook Group</th>
									</tr>
									<tr>
										<td>Offers various university course-specific tutoring</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
									</tr>
									<tr>
										<td>Allows qualified undergraduate students to be tutors</td>
										<td class='green'>&#9745;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
									</tr>
									<tr>
										<td>Sends notifications about local tutors/tutees</td>
										<td class='green'>&#9745;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
									</tr>
									<tr>
										<td>Qualified tutors are constrained to university communities</td>
										<td class='green'>&#9745;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
									</tr>
									<tr>
										<td>Provides real-time scheduling capabilities</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='green'>&#9745;</td>
									</tr>
									<tr>
										<td>Available as a mobile application</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='red'>&#9746;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
									</tr>
									<tr>
										<td>Includes online tutoring options</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='green'>&#9745;</td>
									</tr>
									<tr>
										<td>Includes in-person tutoring options</td>
										<td class='green'>&#9745;</td>
										<td class='red'>&#9746;</td>
										<td class='green'>&#9745;</td>
										<td class='red'>&#9746;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
									</tr>
									<tr>
										<td>Provides informed tutor ratings</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='red'>&#9746;</td>
									</tr>
									<tr>
										<td>Establishes hourly rate ceilings</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='green'>&#9745;</td>
										<td class='red'>&#9746;</td>
									</tr>
									<tr>
										<td>Requires tutor verification/validation to use platform</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
									</tr>
									<tr>
										<td>Does not require subscription commitment</td>
										<td class='green'>&#9745;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='red'>&#9746;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
									</tr>
									<tr>
										<td>Provides 24/7 scheduling</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
										<td class='green'>&#9745;</td>
									</tr>
								</table>
							</div>
							<div class="sidebar">
								<p><span class='green wide text-bg1'>COMPARING US TO OTHERS</span></p>
								<p>It's no surprise that this type of platform already exists in many forms, however, there are many distinctions that we can pick at. Most of our competitors offer at least a few of our desired features, but none possess all of them. This is what distinguishes <span class='gold wide'>Tutor Dash</span> from its competitors.</p><br />
								<p><span class='red wide text-bg1'>DIRECT COMPETITORS</span></p>
								<p>These are competitors that serve the same niche as Tutor Dash. These competitors are all tutor-hosting platforms that try to centralize tutors in one convenient location.</p><br />
								<p><span class='orange wide text-bg1'>INDIRECT COMPETITORS</span></p>
								<p>These are competitors that may serve as competitors to Tutor Dash, as they offer tutor-hosting services, however, these competitors do not primarily serve the niche audience that the direct competitors target.</p><br />
								<div class='img-wrapper'>
									<img src=<?php echo ("'" . $imgDir . "/icons/competition.png'") ?> alt='competitors' />
								</div>
							</div>
						</article>
					</div>
					<br />

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>