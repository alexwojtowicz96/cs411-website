<?php
	$cssDir = "../../../../css";  // relative path of css directory
	$jsDir = "../../../../js";    // relative path of js directory
	$imgDir = "../../../../img";  // relative path of img directory
	$phpDir = "../../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
?>
<!DOCTYPE html>
<html class="subpage risks"> 
	<title>Tutor Dash | Risks</title> 
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "Risk Analysis";  
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class="header">
						<p class='heading'>Addressing The Risks</p>
					</div>
					<div class='bg-deepcove article-has-bg'>
						<article>
							<div class='article'>
								<div class='x-axis'>
									<p>Probability</p>
								</div>
								<div class='risks-wrapper'>
									<div class='y-axis'>
										<p>Impact</p>
									</div>
									<table class='risks'>
										<tr>
											<th></th>
											<th>Very Low</th>
											<th>Low</th>
											<th>Moderate</th>
											<th>High</th>
											<th>Very High</th>
										</tr>
										<tr>
											<td>Very High</td>
											<td class='gold'>T3, T4, L1</td>
											<td class='orange'>T6, L2</td>
											<td class='red'></td>
											<td class='red'>C3, C4</td>
												<td class='red'></td>
										</tr>
										<tr>
											<td>High</td>
											<td class='gold'>T1, C7</td>
											<td class='gold'>C6</td>
											<td class='orange'></td>
											<td class='red'></td>
											<td class='red'></td>
										</tr>
										<tr>
											<td>Moderate</td>
											<td class='green'>C5</td>
											<td class='gold'>T8</td>
											<td class='gold'>C1, C8, T7</td>
											<td class='orange'></td>
											<td class='red'></td>
										</tr>
										<tr>
											<td>Low</td>
											<td class='green'>C2, T5</td>
											<td class='green'></td>
											<td class='gold'>C10, T2</td>
											<td class='gold'></td>
											<td class='orange'></td>
										</tr>
										<tr>
											<td>Very Low</td>
											<td class='green'></td>
											<td class='green'>C9</td>
											<td class='green'></td>
											<td class='gold'></td>
											<td class='gold'></td>
										</tr>
									</table>
								</div>
							</div>
							<div class='sidebar'>
								<div class='vertical-center-wrapper'>
									<div class='vertical-center'>
										<p><span class='gold wide'>THE REALITY</span></p>
										<p>The reality of this situation is that there are all kinds of risks. Here, we've outlined all of the customer, technical, and legal risks associated with developing this project.</p>
									</div>
								</div>
							</div>
						</article>
					</div>
					<section class='risks-card-section'>
<!-- Customer Risks -->
						<div class='risks-card flyin'>
							<div class='visible-section'>
								<div class='overlay-section'>
									<div class='overlay-bg'></div>
									<div class='overlay'>
										<p>Show More</p>
									</div>
								</div>
								<div class='icon-section'>
									<div class='img-wrapper'>
										<img src=<?php echo ("'" . $imgDir . "/icons/customer.png'") ?> alt='customer' />
									</div>
									<div class='title'>
										<p>Customer Risks</p>
									</div>
								</div>
							</div>
							<div class='hidden-section'>
								<div class='table-wrapper'>
									<table class='risks-summary'>
										<tr>
											<th>ID</th>
											<th>Description</th>
											<th>Mitigations</th>
										</tr>
										<tr>
											<td class='gold'>C1</td>
											<td>Student finds the tutor to be unhelpful</td>
											<td>Support of a rating system that indicates this to other customers. Refund money based on circumstances (Business takes a loss).</td>
										</tr>
										<tr>
											<td class='green'>C2</td>
											<td>Prospective tutors faking their qualifications</td>
											<td>Require students to upload their official transcript in PDF format for qualification analysis. This file needs to contain the university's digital signature so not just any any PDF transcript will be considered valid.</td>
										</tr>
										<tr>
											<td class='red'>C3</td>
											<td>Shortage of tutors</td>
											<td>Give small bonuses to tutors for limited time to grow tutor market (Business takes a loss).</td>
										</tr>
										<tr>
											<td class='red'>C4</td>
											<td>Shortage of tutees</td>
											<td>Give free sessions to new members, and give loyalty-free sessions for a certain number of usages (Business takes a loss).</td>
										</tr>
										<tr>
											<td class='green'>C5</td>
											<td>The tutee/tutor leaves a false negative review and/or rating</td>
											<td>Withhold ratings and reviews until both users involved agree that the ratings are justified. Give users the ability to challenge ratings/reviews and require explanations for poor ratings/reviews.</td>
										</tr>
										<tr>
											<td class='gold'>C6</td>
											<td>Users misuse the application; use app maliciously</td>
											<td>Create a terms of service agreement, and blacklist individuals who violate the service agreement.</td>
										</tr>
										<tr>
											<td class='gold'>C7</td>
											<td>Identity theft. Non-users impersonate users and/or users impersonate other users.</td>
											<td>Implement a means authenticating users each time they navigate to a window from outside of the app back into some window inside of the app. This is similar to online banking application methods. Implement a "handshake" agreement where users must confirm their scheduled meeting at the start time.</td>
										</tr>
										<tr>
											<td class='gold'>C8</td>
											<td>Tutor/Tutee doesn't show up to their meeting</td>
											<td>Payment is preallocated. Some deposit is required. Tutors and tutees are both rated, and if one or the other doesn't show up to the meeting then they will receive poor ratings.</td>
										</tr>
										<tr>
											<td class='green'>C9</td>
											<td>Users try to book overlapping sessions</td>
											<td>Only allow users to book appointments for times they do not currently have any session scheduled. This applies to both tutees and tutors.</td>
										</tr>
										<tr>
											<td class='gold'>C10</td>
											<td>Tutors are not adequately prepared to engage with tutees via web conferencing</td>
											<td>Alert users of the minimum requirements for web conference meetings upon selecting 'web conferencing' as a tutoring preference.</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
<!-- Technical Risks -->
						<div class='risks-card flyin-right delay2'>
							<div class='visible-section'>
								<div class='overlay-section'>
									<div class='overlay-bg'></div>
									<div class='overlay'>
										<p>Show More</p>
									</div>
								</div>
								<div class='icon-section'>
									<div class='title'>
										<p>Technical Risks</p>
									</div>
									<div class='img-wrapper'>
										<img src=<?php echo ("'" . $imgDir . "/icons/technical.png'") ?> alt='customer' />
									</div>
								</div>
							</div>
							<div class='hidden-section'>
								<div class='table-wrapper'>
									<table class='risks-summary'>
										<tr>
											<th>ID</th>
											<th>Description</th>
											<th>Mitigations</th>
										</tr>
										<tr>
											<td class='gold'>T1</td>
											<td>Transaction failure. Payment is not received.</td>
											<td>Integrate usage of a 3rd party API designed to handle e-transactions.</td>
										</tr>
										<tr>
											<td class='gold'>T2</td>
											<td>Difficulty automating the process of reading a submitted transcript</td>
											<td>Define reusable code for the general case, and optimize as more information is discovered.</td>
										</tr>
										<tr>
											<td class='gold'>T3</td>
											<td>Database server failure</td>
											<td>Use reliable servers maintained by large corporations (i.e. Google's Firebase).</td>
										</tr>
										<tr>
											<td class='gold'>T4</td>
											<td>Security breach</td>
											<td>Define known security features to prevent unauthorized access.</td>
										</tr>
										<tr>
											<td class='green'>T5</td>
											<td>Some android phones not being able to run application.</td>
											<td>Define minimum SDK for weaker hardware phones, and define normal SDK for standard phones.</td>
										</tr>
										<tr>
											<td class='orange'>T6</td>
											<td>Network server failure</td>
											<td>Server redundancy.</td>
										</tr>
										<tr>
											<td class='gold'>T7</td>
											<td>Pay-rate algorithm does not compute competitive rates for tutors.</td>
											<td>Determine a base pay that will increase/decrease due to various factors. Compare the pay-rates of similarly rated tutors who tutor the same courses.</td>
										</tr>
										<tr>
											<td class='gold'>T8</td>
											<td>Web-conferencing meeting does not get set up properly.</td>
											<td>Schedule events with a Google Hangouts meet on the user's Google Calendar using well-documented Google API's.</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
<!-- Legal Risks -->
						<div class='risks-card flyin delay3'>
							<div class='visible-section'>
								<div class='overlay-section'>
									<div class='overlay-bg'></div>
									<div class='overlay'>
										<p>Show More</p>
									</div>
								</div>
								<div class='icon-section'>
									<div class='img-wrapper'>
										<img src=<?php echo ("'" . $imgDir . "/icons/legal.png'") ?> alt='customer' />
									</div>
									<div class='title'>
										<p>Legal Risks</p>
									</div>
								</div>
							</div>
							<div class='hidden-section'>
								<div class='table-wrapper'>
									<table class='risks-summary'>
										<tr>
											<th>ID</th>
											<th>Description</th>
											<th>Mitigations</th>
										</tr>
										<tr>
											<td class='gold'>L1</td>
											<td>Possible violation of The Family Education Rights and Privacy Act (FERPA).</td>
											<td>Have students explicitly agree to terms of service where they agree to disclose their grades. Transcripts are discarded after eligibility is determined.</td>
										</tr>
										<tr>
											<td class='orange'>L2</td>
											<td>User base uses application for illegal activities</td>
											<td>Define explicitly in terms of service that illegal activities will not be tolerated and any such action will be reported to law enforcement.</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</section>
					<br />

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>