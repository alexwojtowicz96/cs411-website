<?php
	$cssDir = "../../../../css";  // relative path of css directory
	$jsDir = "../../../../js";    // relative path of js directory
	$imgDir = "../../../../img";  // relative path of img directory
	$phpDir = "../../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
?>
<!DOCTYPE html>
<html class="subpage"> 
	<title>Tutor Dash | Current Process Flows</title> 
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "Current Processes"; 
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>Students Seeking Tutors</p>
					</div>
					<div class='bg-deepcove-solid article-has-bg'>
						<article>
							<div class='article'>
								<div class='img-wrapper'>
									<img src=<?php echo ("'" . $imgDir . "/deliverables/current-2.png'") ?> alt='tutor tutee process' />
								</div>
							</div>
							<div class='sidebar'>
								<div class='vertical-center-wrapper'>
									<div class='vertical-center'>
										<p><span class='gold wide'>WHERE DO I LOOK?</span></p>
										<p>Many students who seek tutoring will quickly find that there are an abundance of options that will not suite their needs. This is because there are several factors that determine whether a tutor is compatiable with that student.</p>
										<div class='link-to-img'>
											<a href=<?php echo ("'" . $imgDir . "/deliverables/current-2.png'") ?>>Enlarge Diagram</a>
										</div>
									</div>
								</div>
							</div>
						</article>
					</div>

					<div class='header'>
						<p class='heading'>Students Looking to Tutor</p>
					</div>
					<div class='bg-deepcove-solid article-has-bg'>
						<article>
							<div class='article'>
								<div class='img-wrapper'>
									<img src=<?php echo ("'" . $imgDir . "/deliverables/current-1.png'") ?> alt='tutor tutee process' />
								</div>
							</div>
							<div class='sidebar'>
								<div class='vertical-center-wrapper'>
									<div class='vertical-center'>
										<p><span class='gold wide'>TUTORING ADVERTISEMENT</span></p>
										<p>This process flow diagram highlights the struggles that many students face when trying to advertise their tutoring services. Often, this will lead to students not offering tutoring services simply because it just isn't convenient for them to do so.</p>
										<div class='link-to-img'>
											<a href=<?php echo ("'" . $imgDir . "/deliverables/current-1.png'") ?>>Enlarge Diagram</a>
										</div>
									</div>
								</div>
							</div>
						</article>
					</div>
					<br />

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>