<?php
	$cssDir = "../../../../css";  // relative path of css directory
	$jsDir = "../../../../js";    // relative path of js directory
	$imgDir = "../../../../img";  // relative path of img directory
	$phpDir = "../../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
?>
<!DOCTYPE html>
<html class="subpage"> 
	<title>Tutor Dash | Solution Process Flows</title> 
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "Solution Processes"; 
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>Tutor Dash's Effects</p>
					</div>
					<div class='bg-deepcove-solid article-has-bg'>
						<article>
							<div class='article'>
								<div class='img-wrapper'>
									<img src=<?php echo ("'" . $imgDir . "/deliverables/solution-complicated.png'") ?> alt='solution process' />
								</div>
							</div>
							<div class='sidebar'>
								<div class='vertical-center-wrapper'>
									<div class='vertical-center'>
										<p><span class='gold wide'>A SOLUTION THAT FILLS THE GAPS</span></p>
										<p>With so many options for both students looking to provide tutoring and students looking for tutors, it is no wonder why frustration exists. This diagram illustrates how Tutor Dash affects the current processes. It merges both problems into one process, connects them, and tackles both at once by unifying both participating parties at the source.</p><br />
										<div class='img-wrapper'>
											<img src=<?php echo ("'" . $imgDir . "/icons/merge'") ?> alt='merge' />
										</div><br />
										<div class='link-to-img'>
											<a href=<?php echo ("'" . $imgDir . "/deliverables/solution-complicated.png'") ?>>Enlarge Diagram</a>
										</div>
									</div>
								</div>
							</div>
						</article>
					</div>

					<div class='header'>
						<p class='heading'>At The High Level</p>
					</div>
					<div class='bg-deepcove-solid article-has-bg'>
						<article>
							<div class='article'>
								<div class='img-wrapper'>
									<img src=<?php echo ("'" . $imgDir . "/deliverables/solution-simple.png'") ?> alt='solution process' />
								</div>
							</div>
							<div class='sidebar'>
								<div class='vertical-center-wrapper'>
									<div class='vertical-center'>
										<p><span class='gold wide'>THE GIST OF IT</span></p>
										<p>Essentially, Tutor Dash is a fairly straight-forward appliation. Both participating parties will sign up with the app, and we will do all of the work as far as calculating prices, finding tutors/tutees, and providing payments and communication within our service.</p>
										<div class='link-to-img'>
											<a href=<?php echo ("'" . $imgDir . "/deliverables/solution-simple.png'") ?>>Enlarge Diagram</a>
										</div>
									</div>
								</div>
							</div>
						</article>
					</div>
					<br />

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>