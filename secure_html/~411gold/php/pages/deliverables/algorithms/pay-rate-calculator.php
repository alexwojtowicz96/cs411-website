<?php
	$cssDir = "../../../../css";  // relative path of css directory
	$jsDir = "../../../../js";    // relative path of js directory
	$imgDir = "../../../../img";  // relative path of img directory
	$phpDir = "../../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
?>
<!DOCTYPE html>
<html class="subpage"> 
	<title>Tutor Dash | Pay-Rate Calculator</title> 
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "Pay-Rate Calculator"; 
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>Calculating Competitive Rates</p>
					</div>
					<div class='bg-deepcove-solid article-has-bg'>
						<article>
							<div class='article'>
								<div class='img-wrapper'>
									<img src=<?php echo ("'" . $imgDir . "/deliverables/algorithms/pay-rate.png'") ?> alt='algorithm' />
								</div>
							</div>
							<div class='sidebar'>
								<div class='vertical-center-wrapper'>
									<div class='vertical-center'>
										<p><span class='gold wide'>PURPOSES</span></p>
										<ul class='algorithm-attributes'>
											<li>Keep pay-rates competitive by providing an upper bound based on various factors</li>
											<li>Alleviate the possibility of tutors not getting hired often enough</li>
										</ul>
										<p><span class='gold wide'>PARAMETERS</span></p>
										<ul class='algorithm-attributes'>
											<li>Tutor rating (course-specific)</li>
											<li>Tutor rating (overall)</li>
											<li>Course demand/popularity</li>
											<li>Tutor's experience</li>
											<li>Time of day</li>
											<li>Mean/Standard deviation of rates for courses</li>
											<li>Time since tutor's last request in that course</li>
										</ul>
										<div class='link-to-img'>
											<a href=<?php echo ("'" . $imgDir . "/deliverables/algorithms/pay-rate.png'") ?>>Enlarge Diagram</a>
										</div>
									</div>
								</div>
							</div>
						</article>
					</div>
					<br />

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>