<?php
	$cssDir = "../../../../css";  // relative path of css directory
	$jsDir = "../../../../js";    // relative path of js directory
	$imgDir = "../../../../img";  // relative path of img directory
	$phpDir = "../../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
?>
<!DOCTYPE html>
<html class="subpage"> 
	<title>Tutor Dash | Relative Distance Calculator</title> 
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "Relative Distance"; 
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>Distance Between Users</p>
					</div>
					<div class='bg-deepcove-solid article-has-bg'>
						<article>
							<div class='article'>
								<div class='img-wrapper'>
									<img src=<?php echo ("'" . $imgDir . "/deliverables/algorithms/distance.png'") ?> alt='algorithm' />
								</div>
							</div>
							<div class='sidebar'>
								<div class='vertical-center-wrapper'>
									<div class='vertical-center'>
										<p><span class='gold wide'>PURPOSES</span></p>
										<ul class='algorithm-attributes'>
											<li>Display how far a set of users B is from user A based on user A's search results</li>
											<li>Keep the distance information updated as often as possible</li>
										</ul>
										<p><span class='gold wide'>PARAMETERS</span></p>
										<ul class='algorithm-attributes'>
											<li>Time</li>
											<li>Time interval between updates</li>
											<li>Android device GPS coordinates (User A's and all of User B's longitudes/latitudes)</li>
										</ul>
										<div class='link-to-img'>
											<a href=<?php echo ("'" . $imgDir . "/deliverables/algorithms/distance.png'") ?>>Enlarge Diagram</a>
										</div>
									</div>
								</div>
							</div>
						</article>
					</div>
					<br />

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>