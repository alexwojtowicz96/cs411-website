<?php
	$cssDir = "../../../../css";  // relative path of css directory
	$jsDir = "../../../../js";    // relative path of js directory
	$imgDir = "../../../../img";  // relative path of img directory
	$phpDir = "../../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
?>
<!DOCTYPE html>
<html class="subpage"> 
	<title>Tutor Dash | Payment Logic</title>
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "Payments";
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>Handling Payments</p>
					</div>
					<div class='bg-deepcove-solid article-has-bg'>
						<article>
							<div class='article'>
								<div class='img-wrapper'>
									<img src=<?php echo ("'" . $imgDir . "/deliverables/algorithms/payment-logic.png'") ?> alt='algorithm' />
								</div>
							</div>
							<div class='sidebar'>
								<div class='vertical-center-wrapper'>
									<div class='vertical-center'>
										<p><span class='gold wide'>PURPOSE</span></p>
										<ul class='algorithm-attributes'>
											<li>Calculate payments, handle transactions, and provide logic in the rating system all based on various events that may occur before, during, and after sessions</li>
										</ul>
										<p><span class='gold wide'>TOOLS</span></p>
										<ul class='algorithm-attributes'>
											<li>Braintree API</li>
											<li>Firebase API</li>
										</ul>
										<p><span class='gold wide'>PARAMETERS</span></p>
										<ul class='algorithm-attributes'>
											<li>User A's UID</li>
											<li>User B's UID</li>
											<li>Course ID</li>
											<li>Tutor's pay-rate</li>
											<li>Timestamp for start of session</li>
											<li>Timestamp for end of session</li>
										</ul>
										<div class='link-to-img'>
											<a href=<?php echo ("'" . $imgDir . "/deliverables/algorithms/payment-logic.png'") ?>>Enlarge Diagram</a>
										</div>
									</div>
								</div>
							</div>
						</article>
					</div>
					<br />

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>