<?php
	$cssDir = "../../../../css";  // relative path of css directory
	$jsDir = "../../../../js";    // relative path of js directory
	$imgDir = "../../../../img";  // relative path of img directory
	$phpDir = "../../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
?>
<!DOCTYPE html>
<html class="subpage"> 
	<title>Tutor Dash | MFCD</title>
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "M. F. C. D.";
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>MFCD - Full Implementation</p>
					</div>
					<div class='bg-deepcove-solid article-has-bg'>
						<article>
							<div class='article'>
								<div class='img-wrapper'>
									<img src=<?php echo ("'" . $imgDir . "/deliverables/mfcd.png'") ?> alt='solution process' />
								</div>
							</div>
							<div class='sidebar'>
								<div class='vertical-center-wrapper'>
									<div class='vertical-center'>
										<p><span class='gold wide'>THE BIG PICTURE</span></p>
										<p>This diagram illustrates all of the major functional components of Tutor Dash. This MFCD covers the scope of all universities.</p><br />
										<div class='img-wrapper'>
											<img src=<?php echo ("'" . $imgDir . "/icons/puzzle.png'") ?> alt='diagram' />
										</div><br />
										<div class='link-to-img'>
											<a href=<?php echo ("'" . $imgDir . "/deliverables/mfcd.png'") ?>>Enlarge Diagram</a>
										</div>
									</div>
								</div>
							</div>
						</article>
					</div>

					<div class='header'>
						<p class='heading'>MFCD - Prototype Implementation</p>
					</div>
					<div class='bg-deepcove-solid article-has-bg'>
						<article>
							<div class='article'>
								<div class='img-wrapper'>
									<img src=<?php echo ("'" . $imgDir . "/deliverables/prototype_mfcd.png'") ?> alt='solution process' />
								</div>
							</div>
							<div class='sidebar'>
								<div class='vertical-center-wrapper'>
									<div class='vertical-center'>
										<p><span class='gold wide'>SCALING BACK SLIGHTLY</span></p>
										<p>The major Difference between this MFCD and the full implementation MFCD is that this one only covers ODU as the test case, meaning that the database design will be slightly different.</p><br />
										<div class='img-wrapper'>
											<img src=<?php echo ("'" . $imgDir . "/icons/component1.png'") ?> alt='diagram' />
										</div><br />
										<div class='link-to-img'>
											<a href=<?php echo ("'" . $imgDir . "/deliverables/prototype_mfcd.png'") ?>>Enlarge Diagram</a>
										</div>
									</div>
								</div>
							</div>
						</article>
					</div>
					<br />

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>