<?php
	$cssDir = "../../css";  // relative path of css directory
	$jsDir = "../../js";    // relative path of js directory
	$imgDir = "../../img";  // relative path of img directory
	$phpDir = "../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
?>
<!DOCTYPE html>
<html class="subpage"> <!-- Make sure you append any specific page styles to this class name -->
	<title>Tutor Dash | User Stories</title> <!-- Update this with the page title -->
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "User Stories";  // add banner text here
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>The User Roles</p>
					</div>
					<div class='roles-section'>
						<div class='role-card'>
							<div>
								<p class='title'>Tutee</p>
								<div class='img-wrapper'>
									<img src=<?php echo ("'" . $imgDir . "/icons/tutee.png'") ?> alt='icon' />
								</div>
							</div>
							<ul>
								<li>University student</li>
								<li>Seeks academic assistance</li>
							</ul>
						</div>
						<div class='role-card'>
							<div>
								<p class='title'>Private Tutor</p>
								<div class='img-wrapper'>
									<img src=<?php echo ("'" . $imgDir . "/icons/tutor.png'") ?> alt='icon' />
								</div>
							</div>
							<ul>
								<li>University student</li>
								<li>Qualified to tutor previously taken courses</li>
								<li>Offers tutoring services</li>
								<li>Works independently</li>
							</ul>
						</div>
						<div class='role-card'>
							<div>
								<p class='title'>Tester</p>
								<div class='img-wrapper'>
									<img src=<?php echo ("'" . $imgDir . "/icons/tester.png'") ?> alt='icon' />
								</div>
							</div>
							<ul>
								<li>Team member</li>
								<li>Responsible for quality of software</li>
								<li>Uses tools to write tests for test automation</li>
							</ul>
						</div>
					</div> 
					<br />
					<div class='header'>
						<p class='heading'>The User Stories</p>
					</div>
					<section class='stories'>
						<div class='story-card flyin'>
							<div class='visible-part'>
								<p>As a Tutee...</p>
							</div>
							<div class='hidden-part'>
								<div class='story-part'>
									<p>I need</p>
									<ol>
										<li>...the opportunity to receive tutoring in any class which I am enrolled in.</li>
										<li>...the ability to receive/request tutoring at any time of the day.</li>
										<li>...the ability to message tutors in real-time before hiring them.</li>
										<li>...to search for classes I need help in and receive a list of tutors.</li>
										<li>...payments to be secure and only charged when I can confirm that the meeting did, in fact, occur.</li>
										<li>...the ability to leave meaningful ratings and reviews based on my experiences with tutors.</li>
										<li>...the ability to report malicious activity regarding authorized tutors' activities.</li>
										<li>...the ability to send out alerts so that tutors will be notified when I am looking for them.</li>
										<li>...the ability to confirm that a session did, in fact, occur.</li>
										<li>...to see some indication that tutors are currently available to hire right now.</li>
										<li>...to see the distance a tutor is away from me if I am, in fact, seeking an in-person meeting.</li>
										<li>...the ability to receive tutoring in-person.</li>
										<li>...the ability to receive tutoring online.</li>
										<li>...the ability to search for tutors tutoring any course at my university.</li>
										<li>...the ability to reconnect with a tutor I liked so I can rehire them in the future.</li>
										<li>...tutor profiles to be public when I search for them.</li>
										<li>...sensitive account information to be private and secure.</li>
										<li>...reassurance that the person I am meeting is the person they say they are.</li>
										<li>...registered tutors to be qualified and authorized to tutor me.</li>
										<li>...to search for tutors who attend my university within a certain radius.</li>
										<li>...the ability to report malicious activity regarding authorized tutors' activities.</li>
									</ol>
								</div>
								<div class='story-part'>
									<p>I wish</p>
									<ol>
										<li>...for an appealing visual display both for daytime AND nighttime hours.</li>
										<li>...for my payment methods to be remembered.</li>
										<li>...that if I am searching for a course with no tutors in the system, I will be alerted.</li>
										<li>...for a way of informing Tutor Dash if my desired course is not in the system, so they can alert potential tutors.</li>
										<li>...for a refund on my online session if there are technical difficulties preventing my session from occurring properly.</li>
									</ol>
								</div>
							</div>
						</div>
						<div class='story-card flyin delay2'>
							<div class='visible-part'>
								<p>As a Private Tutor...</p>
							</div>
							<div class='hidden-part'>
								<div class='story-part'>
									<p>I need</p>
									<ol>
										<li>...my qualifications to be based on my previous coursework.</li>
										<li>...to receive notifications when potential tutees message/hire me.</li>
										<li>...my pay-rate to be as competitive as possible so I don't wait too long to get hired.</li>
										<li>...the option to receive alerts when potential tutees in the network are seeking tutoring in courses I tutor.</li>
										<li>...a calendar to maintain and update my availability at any time, which potential tutees can publicly view.</li>
										<li>...the ability for potential tutees to view my user profile.</li>
										<li>...the ability for potential tutees to message me before scheduling a session.</li>
										<li>...my ratings to be given only by tutees which I have tutored.</li>
										<li>...a mechanism for confirming a session did, in fact, occur.</li>
										<li>...payment to be handled within the application.</li>
										<li>...my charging pay-rate to be based partly on my experience.</li>
										<li>...the ability to challenge poor/negative ratings and/or reviews.</li>
										<li>...a mechanism for confirming a session did, in fact, occur.</li>
										<li>...the ability to rate tutees based on my experiences with them.</li>
										<li>...the ability to refuse service.</li>
										<li>...a means of toggling my availability in real-time.</li>
										<li>...web-conferencing and in-person tutoring options.</li>
										<li>...sensitive account information to be private and secure.</li>
									</ol>
								</div>
								<div class='story-part'>
									<p>I wish</p>
									<ol>
										<li>...for a log to keep track of my session and payment history.</li>
										<li>...the option to turn off location services if I am only offering online sessions.</li>
										<li>...for alerts that tell me classes I don't tutor, but am eligible to tutor are in high demand.</li>
										<li>...an alert every X months to remind me to upload an updated transcript so I can tutor more courses.</li>
										<li>...for a bonus to tutor courses in high demand if there is a low supply of tutors.</li>
										<li>...for an alert before a scheduled session occurs if payment for that session fails to go through.</li>
									</ol>
								</div>
							</div>
						</div>
						<div class='story-card flyin delay3'>
							<div class='visible-part'>
								<p>As a Tester...</p>
							</div>
							<div class='hidden-part'>
								<div class='story-part'>
									<p>I need</p>
									<ol>
										<li>...to create a variety of mocked up user accounts with semi-automated decision capabilities to simulate an interactive experience.</li>
										<li>..a visual log that shows me all the attributes and results of my interactive simulation based on events that I initiate.</li>
										<li>...to simulate the signing up both a tutor AND a tutee.</li>
										<li>...to simulate a tutee searching for a tutor.</li>
										<li>...to simulate a tutor searching for a tutee.</li>
										<li>...to mock up data/accounts to simulate various tutors in various courses that exist at ODU.</li>
										<li>...to mock up data/accounts to simulate various tutees who would be using the app.</li>
										<li>...to design a series of test cases where a tutor/tutee is rated at various different times.</li>
										<li>...to design a plan to ensure that pay-rates are affected by weighted ratings, course demand, and time.</li>
										<li>...to simulate a tutee requesting to hire a tutor.</li>
										<li>...to simulate a tutor denying AND a tutor accepting a tutee's request.</li>
										<li>...to simulate a tutee who has sent an alert into the network looking for a tutor.</li>
										<li>...to simulate a tutee who cancels after hiring a tutor.</li>
										<li>...to simulate the transactions that take place before and after an appointment (deposit and payment respectively).</li>
										<li>...to supply a series of mocked up transcripts to the transcript parser that register as official to observe tutoring eligibility based on academic history.</li>
										<li>...to supply fake transcripts to ensure the security of the transcript parser.</li>
										<li>...to simulate a tutor AND tutee query with both inactive and active users existing in the network.</li>
										<li>...to simulate a tutee requesting both an in-person and online meeting.</li>
										<li>...to simulate what happens with payments in the case of when both a tutor and tutee agree to a web conference, but there are technical difficulties.</li>
										<li>...to simulate a user authenticating themselves via email.</li>
										<li>...to simulate the re-authentication process triggered by when a user navigates back into the application from an outside view.</li>
										<li>...to simulate a conversation among two users at two distinctive points in time to make sure chat history is retrieved.</li>
										<li>...to simulate a series of exact queries in which users who are active and appear in the result are moving away/towards the user searching for them.</li>
										<li>...to simulate a meeting in which either (but not both) the tutee or tutor never acknowledge the start of the meeting.</li>
										<li>...to simulate the event in which a tutee gets refunded based on a poor experience.</li>
										<li>...to simulate a scheduling conflict due to the overlap of calendar events.</li>
									</ol>
								</div>
								<div class='story-part'>
									<p>I wish</p>
									<ol>
										<li>...to provide a test case for when users are currently using the application, and the database fails.</li>
										<li>...to run my test suites for every unit of work alongside every build.</li>
										<li>...for a tool to aid in the automation of database querying.</li>
									</ol>
								</div>
							</div>
						</div>
					</section>
					<br />

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>