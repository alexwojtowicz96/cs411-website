<?php
	$cssDir = "../../../../css";  // relative path of css directory
	$jsDir = "../../../../js";    // relative path of js directory
	$imgDir = "../../../../img";  // relative path of img directory
	$phpDir = "../../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
	$presentation_section = (file_get_contents($phpDir . "/partials/presentation-section.php"));
?>
<!DOCTYPE html>
<html class="subpage presentation"> 
	<title>Tutor Dash | Design Version 1.0</title> 
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "Design - Version 1.0"; 
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>Developing Our Solution</p>
					</div>
					<?php 
						$title = "THE INITIAL PLAN";
						$description = "Based on our analysis of the feasibility, we determined that there was a need a platform like Tutor Dash. During this presentatino, we outline some of the major points of Tutor Dash's design and also analyze our different types of risks.";
						$iframe = "https://docs.google.com/presentation/d/e/2PACX-1vTxTMZ7ZylWXLylxyHJgAwAxhKVC02lXZM2sDLy6cC3GAJ7ZZQGGxPQ7s1ZkGwbQ_z-BgGFslKclrX1/embed?start=false&loop=false&delayms=3000";
						$icon = "$imgDir/icons/design-icon.png";
						$pdf = "$phpDir/../pdf/design1.pdf";

						$presentation_content = [
							"title" => $title,
							"description" => $description,
							"iframe" => $iframe,
							"icon" => $icon,
							"pdf" => $pdf,
						];
						echo render_presentation_section($presentation_content, $presentation_section);
					?>

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>