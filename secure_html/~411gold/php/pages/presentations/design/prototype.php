<?php
	$cssDir = "../../../../css";  // relative path of css directory
	$jsDir = "../../../../js";    // relative path of js directory
	$imgDir = "../../../../img";  // relative path of img directory
	$phpDir = "../../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
	$presentation_section = (file_get_contents($phpDir . "/partials/presentation-section.php"));
?>
<!DOCTYPE html>
<html class="subpage presentation"> 
	<title>Tutor Dash | The Prototype</title> 
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "Tutor Dash Prototype";
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>Planning Our Prototype</p>
					</div>
					<?php 
						$title = "MOVING TOWARDS DEVELOPMENT";
						$description = "During this presentation, we highlight the core of Tutor Dash, as these features will be what we end up developing in our prototype. We also provide a tenative schedule as to how we will accomplish our goals.";
						$iframe = "https://docs.google.com/presentation/d/e/2PACX-1vQtPZKGZduBoWwlFq0J-vXIqUq1afKE5Tlt0kqv18RoCdVwOYildtlq6nJCHi4GyUJupD5-F-hSSpsm/embed?start=false&loop=false&delayms=3000";
						$icon = "$imgDir/icons/development.png";
						$pdf = "$phpDir/../pdf/prototype.pdf";

						$presentation_content = [
							"title" => $title,
							"description" => $description,
							"iframe" => $iframe,
							"icon" => $icon,
							"pdf" => $pdf,
						];
						echo render_presentation_section($presentation_content, $presentation_section);
					?>

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>