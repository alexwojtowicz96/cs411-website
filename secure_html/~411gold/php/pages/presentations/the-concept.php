<?php
	$cssDir = "../../../css";  // relative path of css directory
	$jsDir = "../../../js";    // relative path of js directory
	$imgDir = "../../../img";  // relative path of img directory
	$phpDir = "../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
	$presentation_section = (file_get_contents($phpDir . "/partials/presentation-section.php"));
?>
<!DOCTYPE html>
<html class="subpage presentation"> 
	<title>Tutor Dash | The Concept</title> 
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "The Concept"; 
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>How It Began</p>
					</div>
					<?php 
						$title = "THE IDEA";
						$description = "This presentation was put together by Brandon Campbell. In his presentation, he outlines the main premise, purpose, feasibility, and features of Tutor Dash (at the time it was named TutorU). This is how it all began!";
						$iframe = "https://docs.google.com/presentation/d/e/2PACX-1vSE79OwsjSapqVjKgTEwhRYdC7oVU90mYUc2hK28Wu8cEa1rvV2tHXXasPz9TIkSd0eW3t_SXO7ewWZ/embed?start=false&loop=false&delayms=3000";
						$icon = "$imgDir/icons/light-bulb.png";
						$pdf = "$phpDir/../pdf/concept.pdf";

						$presentation_content = [
							"title" => $title,
							"description" => $description,
							"iframe" => $iframe,
							"icon" => $icon,
							"pdf" => $pdf,
						];
						echo render_presentation_section($presentation_content, $presentation_section);
					?>

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>