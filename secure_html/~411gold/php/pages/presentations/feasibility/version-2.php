<?php
	$cssDir = "../../../../css";  // relative path of css directory
	$jsDir = "../../../../js";    // relative path of js directory
	$imgDir = "../../../../img";  // relative path of img directory
	$phpDir = "../../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
	$presentation_section = (file_get_contents($phpDir . "/partials/presentation-section.php"));
?>
<!DOCTYPE html>
<html class="subpage presentation">
	<title>Tutor Dash | Feasibility Version 2.0</title>
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "Feasibility - Version 2.0"; 
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>Broadening Our Scope</p>
					</div>
					<?php 
						$title = "DIVING DEEPER";
						$description = "After the delivery of our original feasibility presentation, we decided to broaden our scope to include all universities into the picture rather than just ODU based on our critique from the experts.";
						$iframe = "https://docs.google.com/presentation/d/e/2PACX-1vR43sXHHOhpBr5mVK2JwPW7OkGtm-9e03PP_wem6WoRXYbMASPOdIYTB4Vm7pggIL1-aTBW1W76q1s8/embed?start=false&loop=false&delayms=3000";
						$icon = "$imgDir/icons/discovery2.png";
						$pdf = "$phpDir/../pdf/feasibility2.pdf";

						$presentation_content = [
							"title" => $title,
							"description" => $description,
							"iframe" => $iframe,
							"icon" => $icon,
							"pdf" => $pdf,
						];
						echo render_presentation_section($presentation_content, $presentation_section);
					?>

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>