<?php
	$cssDir = "../../../../css";  // relative path of css directory
	$jsDir = "../../../../js";    // relative path of js directory
	$imgDir = "../../../../img";  // relative path of img directory
	$phpDir = "../../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
	$presentation_section = (file_get_contents($phpDir . "/partials/presentation-section.php"));
?>
<!DOCTYPE html>
<html class="subpage presentation"> <!-- Make sure you append any specific page styles to this class name if styles on that page need overriding -->
	<title>Tutor Dash | Final Demo</title> <!-- Update this with the page title -->
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "Demonstration 5";  // add banner text here
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>Final Demonstration (Demo 5)</p>
					</div>
					<?php 
						$title = "THE RESULT";
						$description = "This presentation goes over the sum of our development throughout the semester. We did not finsh every feature of the app, but the core has been implemented.";
						$iframe = "https://docs.google.com/presentation/d/e/2PACX-1vQW7sR_p-1QskX_Ij7PfGiqIXnba_uvIpMIbthAJ0OaUWb8BAnqSuOE2Tcy2g9wa0O0S-An3qRevub3/embed?start=false&loop=false&delayms=3000";
						$icon = "$imgDir/icons/td-icon.png";
						$pdf = "$phpDir/../pdf/demonstration-5.pdf"; 

						$presentation_content = [
							"title" => $title,
							"description" => $description,
							"iframe" => $iframe,
							"icon" => $icon,
							"pdf" => $pdf,
						];
						echo render_presentation_section($presentation_content, $presentation_section);
					?>

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>