<?php
	$cssDir = "../../../../css";  // relative path of css directory
	$jsDir = "../../../../js";    // relative path of js directory
	$imgDir = "../../../../img";  // relative path of img directory
	$phpDir = "../../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
	$presentation_section = (file_get_contents($phpDir . "/partials/presentation-section.php"));
?>
<!DOCTYPE html>
<html class="subpage presentation"> <!-- Make sure you append any specific page styles to this class name if styles on that page need overriding -->
	<title>Tutor Dash | Demonstration 4</title> <!-- Update this with the page title -->
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "Demonstration 4";  // add banner text here
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>Formal Demonstration 2 (Demo 4)</p>
					</div>
					<?php 
						$title = "FINISHING UP";
						$description = "This presentation illustrates the new features that we've been working on as we approach the end of the development. It also gives insight as to some of the challenges that we've been facing.";
						$iframe = "https://docs.google.com/presentation/d/e/2PACX-1vSub1AfMu-W_xGvjR0nnPvz6HT3VrPKujO9G5N77_hrHJZC5xwWPa384GiruJqXtS9M4XzJmsQvDFSD/embed?start=false&loop=false&delayms=3000";
						$icon = "$imgDir/icons/new_icon.png";
						$pdf = "$phpDir/../pdf/demonstration-4.pdf"; 

						$presentation_content = [
							"title" => $title,
							"description" => $description,
							"iframe" => $iframe,
							"icon" => $icon,
							"pdf" => $pdf,
						];
						echo render_presentation_section($presentation_content, $presentation_section);
					?>

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>