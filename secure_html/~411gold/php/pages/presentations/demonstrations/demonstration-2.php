<?php
	$cssDir = "../../../../css";  // relative path of css directory
	$jsDir = "../../../../js";    // relative path of js directory
	$imgDir = "../../../../img";  // relative path of img directory
	$phpDir = "../../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
	$presentation_section = (file_get_contents($phpDir . "/partials/presentation-section.php"));
?>
<!DOCTYPE html>
<html class="subpage presentation"> <!-- Make sure you append any specific page styles to this class name if styles on that page need overriding -->
	<title>Tutor Dash | Demonstration 2</title> <!-- Update this with the page title -->
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "Demonstration 2";  // add banner text here
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>Demonstration 2</p>
					</div>
					<?php 
						$title = "PROGRESS CHECK-IN";
						$description = "This presentation summarizes the team's progress on the prototype over the first month and a half. Login and account management are fully-functional. Users can search for other users and view them in real-time.";
						$iframe = "https://docs.google.com/presentation/d/e/2PACX-1vTMtWmCvs1fMx5tNGKUyDXrhHGkm0Whcz6Dz4ifFpozbDs70a6gPlazhIoGtLn_iVzjP4G-YX6gxajm/embed?start=false&loop=false&delayms=3000";
						$icon = "$imgDir/icons/demo.png";
						$pdf = "$phpDir/../pdf/demonstration-2.pdf"; 

						$presentation_content = [
							"title" => $title,
							"description" => $description,
							"iframe" => $iframe,
							"icon" => $icon,
							"pdf" => $pdf,
						];
						echo render_presentation_section($presentation_content, $presentation_section);
					?>

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>