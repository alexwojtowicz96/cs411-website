<?php
	$cssDir = "../../../../css";  // relative path of css directory
	$jsDir = "../../../../js";    // relative path of js directory
	$imgDir = "../../../../img";  // relative path of img directory
	$phpDir = "../../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
	$presentation_section = (file_get_contents($phpDir . "/partials/presentation-section.php"));
?>
<!DOCTYPE html>
<html class="subpage presentation"> <!-- Make sure you append any specific page styles to this class name if styles on that page need overriding -->
	<title>Tutor Dash | Elevator Pitch</title> <!-- Update this with the page title -->
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "Demonstration";  // add banner text here
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>Elevator Pitch</p>
					</div>
					<?php 
						$title = "THE PITCH";
						$description = "This short batch of slides illustrates the process flow for the Tutor Dash development. During this short presentation, you will see how we intend to accomplish most of what we planned out.";
						$iframe = "https://docs.google.com/presentation/d/e/2PACX-1vQC-W09WFZ55cuwpQjwzsLm_N16iISA17J5cmORT1Qg26DbnyYVQTR8yqbakfm4HkXLZp-JwfnmkbAV/embed?start=false&loop=false&delayms=3000";
						$icon = "$imgDir/icons/elevator.png";
						$pdf = "$phpDir/../pdf/elevator-pitch.pdf"; 

						$presentation_content = [
							"title" => $title,
							"description" => $description,
							"iframe" => $iframe,
							"icon" => $icon,
							"pdf" => $pdf,
						];
						echo render_presentation_section($presentation_content, $presentation_section);
					?>

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>