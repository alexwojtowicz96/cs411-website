<?php
	$cssDir = "../../../css";  // relative path of css directory
	$jsDir = "../../../js";    // relative path of js directory
	$imgDir = "../../../img";  // relative path of img directory
	$phpDir = "../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
?>
<!DOCTYPE html>
<html class="subpage"> 
	<title>Tutor Dash | Our Team</title> 
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "Our Team"; 
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">

					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>Meet Team Gold</p>
						<p class='subheading'>Team Gold is a software engineering group from Old Dominion University for CS410 Spring 2019. Team Gold's aim is to make tutoring more centralized across university campuses by connecting students who understand the material with students who don't in real-time 24/7.</p>
					</div>
					<section class="team-container">
						<div class="team-row flyin">
<!-- ALEX -->
							<div class='member-wrapper'>
								<div class='img-wrapper'>
									<img src=<?php echo ("'" .$imgDir . "/team-members/alex.jpg'") ?> alt='gold member' />
								</div>
								<div class="text-wrapper">
									<p class='name'>Alex Wojtowicz</p>
									<p class='role'>Lead/Webmaster<br />DB/Algorithms Developer</p>
									<p class='about'>Alex is a senior majoring in computer science at Old Dominion University. In addition to writing code, he also enjoys music production and graphic design. Upon graduation, Alex hopes to become a full-time web developer.</p>
								</div>
							</div>
<!-- BRANDON -->
							<div class='member-wrapper'>
								<div class='img-wrapper'>
									<img src=<?php echo ("'" .$imgDir . "/team-members/brandon.jpg'") ?> alt='gold member' />
								</div>
								<div class="text-wrapper">
									<p class='name'>Brandon Campbell</p>
									<p class='role'>Database Manager<br />Back-End Developer</p>
									<p class='about'>Brandon is a senior majoring in computer science at Old Dominion University. Before attending ODU, Brandon served six years in the U.S. Navy as an electronics technician. In his free time, he likes to spend time with his wife and enjoy the restaurant and craft beer scene in Hampton Roads. Brandon hopes to pursue a career as an Android developer or as a data engineer.</p>
								</div>
							</div>
<!-- DUNCAN -->
							<div class='member-wrapper'>
								<div class='img-wrapper'>
									<img src=<?php echo ("'" .$imgDir . "/team-members/duncan.png'") ?> alt='gold member' />
								</div>
								<div class="text-wrapper">
									<p class='name'>Duncan Holterhaus</p>
									<p class='role'>Lead Algorithms Developer<br />Back-End Developer</p>
									<p class='about'>Duncan is a senior studying Computer Science at Old Dominion University. In his spare time, he plays music, video games, board games with friends.</p>
								</div>
							</div>
						</div>
						<div class='team-row flyin delay1'>
<!-- DWIGHT -->
							<div class='member-wrapper'>
								<div class='img-wrapper'>
									<img src=<?php echo ("'" .$imgDir . "/team-members/dwight.png'") ?> alt='gold member' />
								</div>
								<div class="text-wrapper">
									<p class='name'>Dwight Owings</p>
									<p class='role'>UI/UX & Algorithms Tester<br />Quality Assurance Expert</p>
									<p class='about'>Dwight Owings was born in Richmond, Virginia and moved to Chesapeake, Virginia in August 2003, only weeks before Hurricane Isabel hit.  Dwight is a computer science major at Old Dominion University who plans to graduate in 2020.</p>
								</div>
							</div>
<!-- JOHN -->
							<div class='member-wrapper'>
								<div class='img-wrapper'>
									<img src=<?php echo ("'" .$imgDir . "/team-members/john.jpg'") ?> alt='gold member' />
								</div>
								<div class="text-wrapper">
									<p class='name'>John Hessefort</p>
									<p class='role'>Developer/Tester<br />Domain Expert</p>
									<p class='about'>John is a tutor at the Math and Science Resource Center on campus. He started the official computer science tutoring program, and finds great pleasure in being able to regularly interact with others and help them with their education. He was recently accepted into the Honors College and Linked BSCS/MS degree program, and has since begun research regarding computational biology.</p>
								</div>
							</div>
<!-- EDWIN -->
							<div class='member-wrapper'>
								<div class='img-wrapper'>
									<img src=<?php echo ("'" .$imgDir . "/team-members/edwin.jpg'") ?> alt='gold member' />
								</div>
								<div class="text-wrapper">
									<p class='name'>Edwin Ordona</p>
									<p class='role'>UI/UX Developer<br />UI/UX Tester</p>
									<p class='about'>Edwin is a senior majoring in computer science at Old Dominion University. His hobbies include music production and playing video games. He plans to pursue a career revolving around his major, leaning heavily towards software or web development.</p>
								</div>
							</div>
						</div>
					</section>

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>