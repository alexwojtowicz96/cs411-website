<?php
	$cssDir = "../../../css";  // relative path of css directory
	$jsDir = "../../../js";    // relative path of js directory
	$imgDir = "../../../img";  // relative path of img directory
	$phpDir = "../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
?>
<!DOCTYPE html>
<html class="subpage"> 
	<title>Tutor Dash | Our Solution</title>
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "Our Solution"; 
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">

					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>Our Solution Statement</p>
						<p class='subheading'>"Tutor Dash is an application-based tutoring service that unifies university students who are interested in tutoring with other university students who desire course-specific tutoring, in real-time. Tutor Dash provides an extension of any university's current tutoring services so that students can receive help for all courses 24/7."</p>
					</div>
					<div class='concept-container'>
						<div class='heading'>
							<p>Characteristics</p>
						</div>
						<div class='wrapper r-padding'>
							<div class='content'>
								<ul>
									<li>Tutor Dash allows qualified students to tutor <span class="gold wide">any class</span> at their university.</li>
									<li>Tutors can make themselves available at any time of the day that is convenient for them.</li>
									<li><span class="gold wide">Tutor Dash consolidates tutors.</span> Whether they work for the university at a tutoring center, or they are private, you can find them using our Tutor Dash.</li>
									<li>Tutoring will be available <span class="gold wide">in-person or online 24/7.</span></li>
									<li><span class="gold wide">Information regarding the student's university's tutoring centers can be maintained more conveniently and in real-time.</span> University-affiliated tutors can use the app to "check-in/out" of the tutoring centers, so that tutees can see if the course they desire tutoring for has a tutor available.</li>
									<li><span class="gold wide">Every tutor is required to provide verification</span> indicating that they are qualified to tutor in any particular course that they wish to provide tutoring services for.</li>
									<li><span class="gold wide">Only university students can use Tutor Dash.</span> Every user needs to have a university-affiliated email address upon creating an account.</li>
									<li>Tutor Dash will provide a <span class="gold wide">mechanism for payments to take place within the application.</span></li>
									<li>Tutor Dash supports the capability for tutors to <span class="gold wide">provide their location</span> and relative distance from tutees.</li>
								</ul>
							</div>
						</div>
					</div>
					<br />

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>