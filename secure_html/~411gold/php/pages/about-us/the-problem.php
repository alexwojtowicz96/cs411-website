<?php
	$cssDir = "../../../css";  // relative path of css directory
	$jsDir = "../../../js";    // relative path of js directory
	$imgDir = "../../../img";  // relative path of img directory
	$phpDir = "../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
	$card1 = (file_get_contents($phpDir . "/partials/card1.php"));
?>
<!DOCTYPE html>
<html class="subpage"> 
	<title>Tutor Dash | The Problem</title> 
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "The Problem"; 
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content">


					<!-- Add content here -->
					<div class='header container'>
						<p class='heading'>Our Problem Statement</p>
						<p class='subheading'>"Tutoring services available to university students are limited in scope, do not provide flexibility, and lack a centralized platform for promotion. Students willing to provide their own tutoring services lack a tool to promote those services."</p>
					</div>
					<div class='concept-container container'>
						<div class='heading'>
							<p>Characteristics</p>
						</div>
						<div class='wrapper r-padding'>
							<div class='content'>
								<ul>
									<li><span class="gold wide">Very limited scope.</span> For example, ODU currently offers tutoring for 132 courses. With over 2400 courses available, this means ODU's tutoring services only cover about 5% of all courses. Over half of these available courses are under 300 level.</li>
									<li><span class="gold wide">Tutoring centers aren't always available.</span> For example, at ODU, campus tutoring services are only available an average of 40 hrs/week. With 168 hours in a week, only about 25% of all available time is being utilized.</li>
									<li><span class="gold wide">University tutoring services are time-restricted</span> to very narrow windows sometimes on only one or two days of the week. Some tutoring is exclusively by-appointment.</li>
									<li><span class="gold wide">Studies suggest that more university students study at night than during the day.</span> Let ODU be an example. The majority of ODU's tutoring services are exclusively offered during the daytime.</li>
									<li><span class="gold wide">Information regarding several universitys' tutoring services is spread over various places</span> on their websites which leads to multiple instances of misinformation.</li>
									<li><span class="gold wide">Current information regarding when and where certain course-specific tutoring sessions is not always reliable</span>, meaning that it is not uncommon for students who arrive at a tutoring center to discover no tutors are available for them.</li>
									<li><span class="gold wide">Students who wish to seek tutoring outside of the university cannot always trust tutors</span> since they are not verified by the university.</li>
									<li><span class="gold wide">There currently doesn't exist an adequate tutoring platform</span> for students who are looking to advertise their own tutoring services</li>
								</ul>
							</div>
						</div>
					</div>

					<?php 
						$firstCard = [
							"title" => "Decentralized Resources",
							"blurb" => "University tutoring is decentralized, there is a shortage of tutors and there is a surplus of underutilized student resources.",
							"image" => "$imgDir/resources.jpg",
							"link" => "#",
						];
						$secondCard = [
							"title" => "Advertising Difficulties",
							"blurb" => "Students looking to advertise their own tutoring resources have difficulties finding the best platform to do so.",
							"image" => "$imgDir/looking.jpg",
							"link" => "#",
						];
						$thirdCard = [
							"title" => "Endless Frustration",
							"blurb" => "Students have difficulties finding, both university and private, tutors that best suit their specific needs.",
							"image" => "$imgDir/struggle.png",
							"link" => "#",
						];
						$cards = [
							$firstCard,
							$secondCard,
							$thirdCard,
						];
						echo render_card1_section($cards, $phpDir, $imgDir, $card1); 
					?>

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>