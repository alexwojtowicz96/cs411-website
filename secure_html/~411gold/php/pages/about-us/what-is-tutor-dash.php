<?php
	$cssDir = "../../../css";
	$jsDir = "../../../js";
	$imgDir = "../../../img";
	$phpDir = "../../../php";
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
?>
<!DOCTYPE html>
<html class="subpage">
	<title>Tutor Dash | What is Tutor Dash?</title>
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "What is Tutor Dash?";
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">

					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>What We Are About</p>
						<p class='subheading'>We are making tutoring more centralized and convenient for university students. Tutor Dash is a mobile application that merges the current tutoring resources at several universities with the current process flows of students seeking tutoring and students seeking advertisement for tutoring into one convenient tool. With such a platform, finding a tutor and being a tutor has never been this easy!</p>
					</div>
					<div class='concept-container'>
						<div class='heading'>
							<p>The Concept</p>
						</div>
						<div class='wrapper'>
							<div class='left-column'>
								<div class='content'>
									<ul>
										<li>There are various instances where students need academic assistance.</li>
										<li>Universities provide tutoring services, but sometimes they don't suffice.</li>
										<li>Many students attending university qualify to be tutors, but often, they aren't employed by the university.</li>
										<li>Tutor Dash combines all of these factors into one solution. By allowing students to become tutors, this makes tutoring more appealing, convenient, and widespread for all parties involved!</li>
									</ul>
								</div>
							</div>
							<div class='right-column'>
								<p class='university'>University</p>
								<p class='seeking-tutor'>Students Seeking Academic Assistance</p>
								<p class='seeking-tutee'>Students Seeking Tutoring Advertisement</p>
								<p class='tutor-dash'>Tutor Dash</p>
								<div class='img-wrapper'>
									<img src=<?php echo ("'" . $imgDir . "/icons/the-concept.png'") ?> alt='tech tutor concept' />
								</div>
							</div>
						</div>
					</div>
					<br />

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>