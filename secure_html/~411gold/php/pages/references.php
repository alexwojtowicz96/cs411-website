<?php
	$cssDir = "../../css";  // relative path of css directory
	$jsDir = "../../js";    // relative path of js directory
	$imgDir = "../../img";  // relative path of img directory
	$phpDir = "../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
?>
<!DOCTYPE html>
<html class="subpage"> 
	<title>Tutor Dash | References</title> 
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "References"; 
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>Our Resources</p>
					</div>
					<section class='resources'>
						<div class='section'>
							<h3>University Tutoring Resources</h3>
							<ol>
								<li>"Academics." Old Dominion University, 8 Jan. 2019. URL: 
									<a href= 'http://www.odu.edu/academics' target='_blank'>www.odu.edu/academics</a>
								</li>
								<li>"Campus Tutoring." Old Dominion University, 19 Jan. 2019. URL: 
									<a href='http://www.odu.edu/success/academic/tutoring#tab125=0' target='_blank'>www.odu.edu/success/academic/tutoring#tab125=0</a>
								</li>
								<li>"Course-Specific Tutoring." Old Dominion University, 19 Jan. 2019. URL: 
									<a href='http://www.odu.edu/success/academic/tutoring/course-specific' target='_blank'>www.odu.edu/success/academic/tutoring/course-specific</a>
								</li>
								<li>"Courses of Instruction." Old Dominion University, Feb. 2019. URL: 
									<a href='http://catalog.odu.edu/courses/' target='_blank'>catalog.odu.edu/courses/</a>
								</li>
								<li>"Academic Tutoring in Comprehensive Universities." Hanover Research, 2014. URL: 
									<a href='https://www.hanoverresearch.com/wp-content/uploads/2017/08/Academic-Tutoring-in-Comprehensive-Universities.pdf' target='_blank'>https://www.hanoverresearch.com/wp-content/uploads/2017/08/Academic-Tutoring-in-Comprehensive-Universities.pdf</a>
								</li>
							</ol>
						</div>
						<div class='section'>
							<h3>Student Behaviors</h3>
							<ol>
								<li>Ciscell, Galen, et al. "Barriers to Accessing Tutoring Services Among Students Who Received a MidSemester Warning." ERIC, Pacific Lutheran University - Department of Sociology, 2016. URL: 
									<a href='http://files.eric.ed.gov/fulltext/EJ1114513.pdf' target='_blank'>files.eric.ed.gov/fulltext/EJ1114513.pdf</a>
								</li>
								<li>Evans MDR, Kelley P and Kelley J (2017). Identifying the Best Times for Cognitive Functioning Using New Methods: Matching University Times to Undergraduate Chronotypes. Front. Hum. Neurosci. 11:188. doi: 10.3389/fnhum.2017.00188. URL: 
									<a href='https://www.frontiersin.org/articles/10.3389/fnhum.2017.00188/full?&utm_source=Email_to_authors_&utm_medium=Email&utm_content=T1_11.5e1_author&utm_campaign=Email_publication&field=&journalName=Frontiers_in_Human_Neuroscience&id=239492' target='_blank'>https://www.frontiersin.org/articles/10.3389/fnhum.2017.00188/full?&utm_source=Ema...</a>
								</li>
								<li>Fry, Natalie. "New Research Reveals That College Students Study Best Later in the Day." NevadaToday, University of Nevada, Reno, 11 Apr. 2017. URL: 
									<a href='http://www.unr.edu/nevada-today/news/2017/best-time-of-day-to-study' target='_blank'>www.unr.edu/nevada-today/news/2017/best-time-of-day-to-study</a>
								</li>
								<li>"Peer Assisted Learning" BMC Education, 8 March 2006 URL: 
									<a href='https://bmcmededuc.biomedcentral.com/articles/10.1186/1472-6920-6-18' target='_blank'>https://bmcmededuc.biomedcentral.com/articles/10.1186/1472-6920-6-18</a>
								</li>
								<li>Pierce, Dennis. "Supporting Students Beyond Financial Aid", 2016 URL: 
									<a href='http://eds.b.ebscohost.com.proxy.lib.odu.edu/ehost/detail/detail?vid=0&sid=d93df6c4-3729-4b62-8d58-95e25c309878%40sessionmgr102&bdata=JnNpdGU9ZWhvc3QtbGl2ZSZzY29wZT1zaXRl#AN=114789419&db=ehh' target='_blank'>http://eds.b.ebscohost.com.proxy.lib.odu.edu/ehost/detail/detail?...</a>
								</li>
								<li>Qayyum, Adnan. "Student Help-Seeking Attitudes and Behaviors in a Digital Era." International Journal of Educational Technology in Higher Education, vol. 15, no. 1, 2018, doi:10.1186/s41239-018-0100-7. URL: 
									<a href='https://educationaltechnologyjournal.springeropen.com/articles/10.1186/s41239-018-0100-7' target='_blank'>https://educationaltechnologyjournal.springeropen.com/articles/10.1186/s41239-018-0100-7</a>
								</li>
								<li>"Student as Peer Tutors" BMC Education, 9 June. 2014. URL: https://bmcmededuc.biomedcentral.com/articles/10.1186/1472-6920-14-115
									<a href='https://bmcmededuc.biomedcentral.com/articles/10.1186/1472-6920-14-115' target='_blank'>https://bmcmededuc.biomedcentral.com/articles/10.1186/1472-6920-14-115</a>
								</li>
								<li>Topping, J. Keith. "Trends in Peer Learning", 19 Jan 2007 URL: 
									<a href='https://www.tandfonline.com/doi/full/10.1080/01443410500345172?scroll=top&needAccess=tru' target='_blank'>https://www.tandfonline.com/doi/full/10.1080/01443410500345172?scroll=top&needAccess=tru</a>
								</li>
							</ol>
						</div>
						<div class='section'>
							<h3>Competition</h3>
							<ol>
								<li>"Facebook - Groups." Facebook Help Center, Facebook, 2019. URL: 
									<a href='http://www.facebook.com/help/1629740080681586?helpref=hc_global_nav' target='_blank'>www.facebook.com/help/1629740080681586?helpref=hc_global_nav</a>
								</li>
								<li>"Find a Local In-Home Tutor Today." HeyTutor, HeyTutor LLC. URL: 
									<a href='http://heytutor.com/' target='_blank'>heytutor.com/</a>
								</li>
								<li>"Skooli Tutors Online." Skooli Online Tutoring, Skooli, Feb. 2019. URL: 
									<a href='http://www.skooli.com/prices/students' target='_blank'>www.skooli.com/prices/students</a>
								</li>
								<li>"Tutor Matching Service - How It Works." Tutor Matching Service, Tutor Matching Service, 2019. URL: 
									<a href='http://tutormatchingservice.com/#/about' target='_blank'>tutormatchingservice.com/#/about</a>
								</li>
								<li>"Tutors - Care.com." Care.com, Care.com, Feb. 2019. URL: 
									<a href='http://www.care.com/tutors' target='_blank'>www.care.com/tutors</a>
								</li>
								<li>"Tutor.com - The Princeton Review." Tutor.com, The Princeton Review, URL: 
									<a href='http://www.tutor.com' target='_blank'>www.tutor.com</a>
								</li>
								<li>"Wyzant." Wyzant Resources, Wyzant Inc., Feb. 2019. URL: 
									<a href='http://www.wyzant.com/howitworks/students' target='_blank'>www.wyzant.com/howitworks/students</a>
								</li>
							</ol>
						</div>
					</section>
					<br />

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>