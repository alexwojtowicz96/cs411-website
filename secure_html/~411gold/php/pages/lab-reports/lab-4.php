<?php
	$cssDir = "../../../css";  // relative path of css directory
	$jsDir = "../../../js";    // relative path of js directory
	$imgDir = "../../../img";  // relative path of img directory
	$phpDir = "../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
?>
<!DOCTYPE html>
<html class="subpage lab-report presentation">
	<title>Tutor Dash | Lab Report 4</title> 
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "Lab Report 4"; 
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>Gitlab Wiki</p>
					</div>
					<div style="text-align: center;">
						<a href='https://git-community.cs.odu.edu/bcampbe/cs411-gold/wikis/home' target='_blank' class='gitlab'>
							<img style='width: 270px; height: 270px;' src='../../../img/icons/gitlab.png' alt='gitlab wiki'>
						</a>
					</div>

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>