<?php
	$cssDir = "../../../css";  // relative path of css directory
	$jsDir = "../../../js";    // relative path of js directory
	$imgDir = "../../../img";  // relative path of img directory
	$phpDir = "../../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
	$team_lab_section = (file_get_contents($phpDir . "/partials/team-lab-section.php"));
?>
<!DOCTYPE html>
<html class="subpage lab-report presentation">
	<title>Tutor Dash | Lab Report 2</title> 
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "Lab Report 2"; 
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div class='header'>
						<p class='heading'>Individual Reports</p>
					</div>
					<p style="background-color: #C70000; display: block; padding: 10px; margin-bottom: 10px;">Version 1</p>
					<div style="text-align: center;">
						<a style="color: #FFFFFF; display: inline-block; letter-spacing: .1em; font-size: 18px; margin-bottom: 6px; color: #D6C025;" href="../../../pdf/labs/lab2/v1/collab.pdf">Collaborative Section 3</a>
					</div>
					<?php
						$labsDir = $phpDir . "/../pdf/labs/lab2/";
						$lab_paths = [
							"alexlab" => $labsDir . "v1/alex.pdf",
							"brandonlab" => $labsDir . "v1/brandon.pdf",
							"duncanlab" => $labsDir . "v1/duncan.pdf",
							"dwightlab" => $labsDir . "v1/dwight.pdf",
							"johnlab" => $labsDir . "v1/john.pdf",
							"edwinlab" => $labsDir . "v1/edwin.pdf",
						];
						$icon = $imgDir . "/icons/pdf-icon.png";
						echo render_team_lab_section($lab_paths, $icon, $team_lab_section);
					?>
					<p style="background-color: #C70000; display: block; padding: 10px; margin-bottom: 10px; margin-top: 10px;">Version 2</p>
					<div style="text-align: center;">
						<a style="color: #FFFFFF; display: inline-block; letter-spacing: .1em; font-size: 18px; margin-bottom: 6px; color: #D6C025;" href="../../../pdf/labs/lab2/v2/collab.pdf">Collaborative Section 3</a>
					</div>
					<?php
						$lab_paths2 = [
							"alexlab" => $labsDir . "v2/alex.pdf",
							"brandonlab" => $labsDir . "v2/brandon.pdf",
							"duncanlab" => $labsDir . "v2/duncan.pdf",
							"dwightlab" => $labsDir . "v2/dwight.pdf",
							"johnlab" => $labsDir . "v2/john.pdf",
							"edwinlab" => $labsDir . "v2/edwin.pdf",
						];
						echo render_team_lab_section($lab_paths2, $icon, $team_lab_section);
					?>
					<p style="background-color: #C70000; display: block; padding: 10px; margin-bottom: 10px; margin-top: 10px;">Version 3</p>
					<div style="text-align: center;">
						<a style="color: #FFFFFF; display: inline-block; letter-spacing: .1em; font-size: 18px; margin-bottom: 6px; color: #D6C025;" href="../../../pdf/labs/lab2/v3/collab.pdf">Collaborative Section 3</a>
					</div>
					<br />

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>