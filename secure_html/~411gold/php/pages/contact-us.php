<?php
	$cssDir = "../../css";  // relative path of css directory
	$jsDir = "../../js";    // relative path of js directory
	$imgDir = "../../img";  // relative path of img directory
	$phpDir = "../../php";  // relative path of php directory
	
	include ($phpDir . "/modules/helpers.php");
	include ($phpDir . "/modules/contact-form.php");
	$head = (file_get_contents($phpDir . "/partials/head.php"));
	$nav = (file_get_contents($phpDir . "/partials/navigation.php"));
	$banner = (file_get_contents($phpDir . "/partials/banner.php"));
	$footer = (file_get_contents($phpDir . "/partials/footer.php"));
	$scripts = (file_get_contents($phpDir . "/partials/scripts.php"));
?>
<!DOCTYPE html>
<html class="subpage contact-us"> 
	<title>Tutor Dash | Contact Us</title> 
	<head>
		<?php 
			echo get_header_section($head, $cssDir);
		?>
	</head>
	<body>
		<nav>
			<?php 
				echo get_nav_section($nav, $phpDir, $imgDir);
			?>
		</nav>
		<section>
			<?php 
				$bannerContent = "Contact Us";
				echo get_banner_section($banner, $imgDir, $bannerContent);
			?>
		</section>
		<main>
			<div class="body">
				<div class="content container">


					<!-- Add content here -->
					<div id="form-main">
            			<div id="form-div">
            				<article>
            					<div class='article'>
                			<form class="form" action="<?= $_SERVER['PHP_SELF']; ?>" id="form1" method="post">
                    			<p class="name">
                        			<input name="fname" type="text" class="validate[required,custom[onlyLetter],length[0,100]] feedback-input" placeholder="First Name*" id="name" value="<?= $fname ?>">
                        			<span class="error"><?= $fname_error ?></span>
                    			</p>
                    			<p class="name">
                        			<input name="lname" type="text" class="validate[required,custom[onlyLetter],length[0,100]] feedback-input" placeholder="Last Name*" id="name" value="<?= $lname ?>">
                        			<span class="error"><?= $lname_error ?></span>
                    			</p>
                    			<p class="email">
                        			<input name="email" type="text" class="validate[required,custom[email]] feedback-input" id="email" placeholder="Email*" value="<?= $email ?>">
                        			<span class="error"><?= $email_error ?></span>
                    			</p>
                    			<p class="text">
                        			<textarea name="msg" type="text" class="validate[required,length[6,300]] feedback-input" id="comment" placeholder="Message*" value="<?= $msg ?>"></textarea>
                        			<span class="error"><?= $msg_error ?></span>
                    			</p>
                   				<div class="submit">
                        			<input type="submit" value="SEND" id="button-blue">
                        			<div class="ease"></div>
                    			</div>
                    			<div class="success"><?= $success ?></div>
                			</form>
                				</div>
                				<div class='sidebar'>
                					<p><span class='gold wide'>WE'RE HERE FOR YOU</span>
                					</p>
                					<p>Feel free to contact us if you have any questions or concerns regarding Tutor Dash. We will get back to you as soon as possible!</p>
                					<div class='img-wrapper'>
                						<img src=<?php echo ("'" . $imgDir . "/icons/contact.png'") ?> alt='help' />
                					</div>
                				</div>
                			</article>
            			</div>
        			</div>

				</div>
			</div>
		</main>
		<footer>
			<?php 
				echo get_section_with_images($footer, $imgDir);
			?>
		</footer>
		<?php 
			echo get_script_section($scripts, $jsDir);
		?>
	</body>
</html>